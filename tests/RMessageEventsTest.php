<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Abstraction\Events\MessageStatusChanged;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\RetailCRM\AppHandler;
use BmPlatform\RetailCRM\Utils\Entities\Helpers\Message;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use Carbon\Carbon;
use Mockery as m;

class RMessageEventsTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    protected function setUp(): void
    {
        \Illuminate\Support\Facades\Config::swap($config = m::mock(\Illuminate\Contracts\Config\Repository::class));

        $config->expects('get')->with('retailcrm.applications')->andReturn([ 'main' => ['botName' => 'mybot' ]])->zeroOrMoreTimes();
        $config->expects('get')->with('retailcrm_params.required')->andReturn([])->zeroOrMoreTimes();
        $config->expects('get')->with('retailcrm.messenger_types.telegram.internal_type')->andReturn('telegram')->zeroOrMoreTimes();
        $config->expects('get')->with('retailcrm.messenger_types.whatsapp.internal_type')->andReturn('whatsapp')->zeroOrMoreTimes();
        $config->expects('get')->with('retailcrm.messenger_types.vk.internal_type')->andReturn('vk')->zeroOrMoreTimes();

        \Illuminate\Support\Facades\Lang::expects('get')->withAnyArgs()->andReturn('trans')->zeroOrMoreTimes();
    }

    public function testInbox()
    {
        $dataText = json_decode(file_get_contents(__DIR__ . '/SampleData/WH/message_new_received.json'), true);
        $dataTextCreatesChat = json_decode(
            file_get_contents(__DIR__ . '/SampleData/WH/message_new_received_new_chat.json'),
            true
        );
        $dataTextCreatesDialog = json_decode(
            file_get_contents(__DIR__ . '/SampleData/WH/message_new_received_new_dialog.json'),
            true
        );
        $dataTextWithAttachment = json_decode(
            file_get_contents(__DIR__ . '/SampleData/WH/message_new_received_with_attachment.json'),
            true
        );
        $customerMainApiData = json_decode(file_get_contents(__DIR__ . '/SampleData/API/customer.json'), true);
        $fileData = json_decode(file_get_contents(__DIR__ . '/SampleData/API/file.json'), true);


        $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);

        $mainApi = m::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $botApi = m::mock(\BmPlatform\RetailCRM\BotApiClient::class);
        $botApi->expects('get')->with(
            'files/:file-id',
            ['params' => ['file-id' => '779f9137-c62f-4a38-a479-8a65bd59acd8']]
        )->andReturn(
            $fileData
        )->times(1);

        $module = m::mock(AppHandler::class, [
            $u,
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $module->expects('getBotApiClient')->andReturn($botApi)->times(1);

        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [$module, $dataText]);

        $extraFields = null;

        $extraData = [
        ];

        $data->makePartial();

        $expectedChat = new Chat(
            externalId: 1,
            messengerInstance: new \BmPlatform\Abstraction\DataTypes\MessengerInstance(
                'telegram', 3, '@rcrmtestbot_bot',
                messengerId: 'rcrmtestbot_bot',
                internalType: \BmPlatform\Abstraction\Enums\MessengerType::Telegram
            ),
            contact: new Contact(
                externalId: 5,
                name: "Bogdan Shipilov",
                avatarUrl: "https://mg-transport-content.s3.eu-central-1.amazonaws.com/telegram/a78cd8989ad9f7f6ac6a33cde689bae2e42cf5322b9302cf8dd43525c324ee03.jpg",
                messengerId: '',
                extraData: [
                    ExtraDataProps::USERNAME => 'theBodyan',
                    ExtraDataProps::FIRST_NAME => 'Bogdan',
                    ExtraDataProps::LAST_NAME => 'Shipilov',
                ],
            ),
            operator: $operator = new \BmPlatform\Abstraction\DataTypes\Operator(
                externalId: '8',
                name: 'Bogdan manager',
                extraData: [
                    ExtraDataProps::ACCOUNT_TYPE => 'user',
                    ExtraDataProps::MAIN_API_ID => '15',
                    ExtraDataProps::FIRST_NAME => 'Bogdan manager',
                    ExtraDataProps::LAST_NAME => '',
                ]
            ),
            extraData: [ExtraDataProps::LAST_DIALOG_ID => 20]
        );
        $expectedContact = new Contact(
            externalId: 5,
            name: 'Bogdan Shipilov',
            avatarUrl: 'https://mg-transport-content.s3.eu-central-1.amazonaws.com/telegram/a78cd8989ad9f7f6ac6a33cde689bae2e42cf5322b9302cf8dd43525c324ee03.jpg',
            extraFields: $extraFields,
            messengerId: '',
            extraData: [
                ExtraDataProps::USERNAME => 'theBodyan',
                ExtraDataProps::LAST_NAME => 'Shipilov',
                ExtraDataProps::FIRST_NAME => 'Bogdan',
            ],
        );
        $expectedMessage = new MessageData(
            externalId: 250,
            text: ';;;;;;;;;;;;;;;;;;;;;;',
            attachments: null,
            date: Carbon::parse('2022-10-12T04:02:03.345350763Z'),
            extraData: [
                ExtraDataProps::EXTERNAL_MESSAGE_STATUS => 'received'
            ]
        );

        $inbox = new \BmPlatform\RetailCRM\EventHandlers\MessageNew();

        $this->assertEquals(
            new InboxReceived(
                chat: $expectedChat,
                participant: $expectedContact,
                message: $expectedMessage,
                flags: 0,
                timestamp: $data->timestamp()
            ),
            $inbox($data)
        );

        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [$module, $dataTextCreatesChat]);

        $data->makePartial();

        $this->assertEquals(
            new InboxReceived(
                chat: $expectedChat,
                participant: $expectedContact,
                message: $expectedMessage,
                flags: 3,
                timestamp: $data->timestamp()
            ),
            $inbox($data)
        );

        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [$module, $dataTextCreatesDialog]);

        $data->makePartial();

        $this->assertEquals(
            new InboxReceived(
                chat: $expectedChat,
                participant: $expectedContact,
                message: $expectedMessage,
                flags: 1,
                timestamp: $data->timestamp()
            ),
            $inbox($data)
        );

        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [$module, $dataTextWithAttachment]);

        $data->makePartial();

        $expectedMessage = new MessageData(
            externalId: 251,
            text: 'wppr',
            attachments: [
                new \BmPlatform\Abstraction\DataTypes\MediaFile(
                    type: \BmPlatform\Abstraction\Enums\MediaFileType::Image,
                    url: 'https://storage.yandexcloud.net/mg-node-files/files/18528/779f9137-c62f-4a38-a479-8a65bd59acd8',
                    mime: 'image/jpeg'
                )
            ],
            date: Carbon::parse('2022-10-12T04:02:50.779414Z'),
            extraData: [
                ExtraDataProps::EXTERNAL_MESSAGE_STATUS => 'received'
            ]
        );

        $this->assertEquals(
            new InboxReceived(
                chat: $expectedChat,
                participant: $expectedContact,
                message: $expectedMessage,
                flags: 0,
                timestamp: $data->timestamp()
            ),
            $inbox($data)
        );
    }

    public function testOutbox()
    {

        $dataText = json_decode(file_get_contents(__DIR__ . '/SampleData/WH/message_new_sent.json'), true);
        $dataTextFromOurBot = json_decode(
            file_get_contents(__DIR__ . '/SampleData/WH/message_new_sent_by_our_bot.json'),
            true
        );

        $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);

        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [
            m::mock(AppHandler::class, [
                $u,
                \Illuminate\Support\Facades\Config::getFacadeRoot(),
            ]),
            $dataText
        ]);

        $data->makePartial();

        $outbox = new \BmPlatform\RetailCRM\EventHandlers\MessageNew();

        $date = Carbon::parse('2022-10-12T03:58:39.889497Z');

        $this->assertEquals(
            new OutboxSent(
                chat: new Chat(
                    externalId: 1,
                    messengerInstance: new \BmPlatform\Abstraction\DataTypes\MessengerInstance(
                        'telegram', 3, '@rcrmtestbot_bot',
                        messengerId: 'rcrmtestbot_bot',
                        internalType: \BmPlatform\Abstraction\Enums\MessengerType::Telegram
                    ),
                    contact: new Contact(
                        externalId: 5,
                        name: "Bogdan Shipilov",
                        avatarUrl: "https://mg-transport-content.s3.eu-central-1.amazonaws.com/telegram/a78cd8989ad9f7f6ac6a33cde689bae2e42cf5322b9302cf8dd43525c324ee03.jpg",
                        messengerId: '',
                        extraData: [
                            ExtraDataProps::USERNAME => 'theBodyan',
                            ExtraDataProps::LAST_NAME => 'Shipilov',
                            ExtraDataProps::FIRST_NAME => 'Bogdan',
                        ],
                    ),
                    operator: $operator = new \BmPlatform\Abstraction\DataTypes\Operator(
                        externalId: '1',
                        name: 'Bogdan',
                        extraData: [
                            ExtraDataProps::MAIN_API_ID => '13',
                            ExtraDataProps::FIRST_NAME => 'Bogdan',
                            ExtraDataProps::ACCOUNT_TYPE => 'user',
                            ExtraDataProps::LAST_NAME => null,
                        ]
                    ),
                    extraData: [ExtraDataProps::LAST_DIALOG_ID => 20]
                ),
                message: new MessageData(
                    externalId: 248,
                    text: 'gologoglo',
                    attachments: null,
                    date: $date,
                    extraData: [
                        ExtraDataProps::EXTERNAL_MESSAGE_STATUS => 'sending'
                    ]
                ),
                operator: $operator,
                timestamp: $data->timestamp(),
            ),
            $outbox($data)
        );

        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [
            m::mock(AppHandler::class, [
                $u,
                m::mock(\Illuminate\Contracts\Config\Repository::class),
            ]),
            $dataTextFromOurBot
        ]);
        $data->makePartial();


        $this->assertEquals(
            null,
            $outbox($data)
        );
    }

    /** @dataProvider dataProvider */
    public function testEvents($file, $response): void
    {
        $data = json_decode(file_get_contents(__DIR__ . '/SampleData/WH/'.$file.'.json'), true);

        $app = $this->mockAppInstance();

        $handler = m::mock(AppHandler::class, [
            $app, \Illuminate\Support\Facades\Config::getFacadeRoot(),
        ]);

        $app->expects('getHandler')->andReturn($handler)->zeroOrMoreTimes();

        $eventHandler = new \BmPlatform\RetailCRM\EventHandler();
        $dataWrap = new \BmPlatform\RetailCRM\Utils\DataWrap($handler, $data);

        $this->assertEquals($response, \Illuminate\Support\Arr::first($eventHandler->getEvents($dataWrap)));
    }

    public function mockAppInstance(): \BmPlatform\Abstraction\Interfaces\AppInstance|m\MockInterface
    {
        static $value;

        return isset($value) ? $value : $value = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
    }

    public function dataProvider(): array
    {
        return [
            [
                'sent_message_updated',
                new MessageStatusChanged(
                    chat: new Chat(
                        externalId: 1,
                        messengerInstance: new \BmPlatform\Abstraction\DataTypes\MessengerInstance(
                            'telegram', 3, '@rcrmtestbot_bot',
                            messengerId: 'rcrmtestbot_bot',
                            internalType: \BmPlatform\Abstraction\Enums\MessengerType::Telegram
                        ),
                        contact: new Contact(
                            externalId: 5,
                            name: "Bogdan Shipilov",
                            avatarUrl: "https://mg-transport-content.s3.eu-central-1.amazonaws.com/telegram/a78cd8989ad9f7f6ac6a33cde689bae2e42cf5322b9302cf8dd43525c324ee03.jpg",
                            messengerId: '',
                            extraData: [
                                ExtraDataProps::USERNAME => 'theBodyan',
                                ExtraDataProps::LAST_NAME => 'Shipilov',
                                ExtraDataProps::FIRST_NAME => 'Bogdan',
                            ],
                        ),
                        operator: new \BmPlatform\Abstraction\DataTypes\Operator(
                            externalId: '1',
                            name: 'Bogdan',
                            extraData: [
                                ExtraDataProps::ACCOUNT_TYPE => 'user',
                                ExtraDataProps::MAIN_API_ID => '13',
                                ExtraDataProps::FIRST_NAME => 'Bogdan',
                                ExtraDataProps::LAST_NAME => null,
                            ]
                        ),
                        extraData: [ExtraDataProps::LAST_DIALOG_ID => 20]
                    ),
                    externalId: 248,
                    status: \BmPlatform\Abstraction\Enums\MessageStatus::Sent,
                    timestamp: Carbon::createFromTimestamp(1665547120),
                ),
            ],

            [
                'message_new_sending_order',
                new OutboxSent(
                    chat: new Chat(
                        externalId: '32503',
                        messengerInstance: new \BmPlatform\Abstraction\DataTypes\MessengerInstance(
                            externalType: 'whatsapp',
                            externalId: '60',
                            name: 'Выпускные Ленты (+7 958 581-54-26)',
                            messengerId: '79585815426',
                            internalType: \BmPlatform\Abstraction\Enums\MessengerType::Whatsapp,
                        ),
                        contact: new Contact(
                            externalId: '32501',
                            name: '31414',
                            phone: '79092182926',
                            avatarUrl: 'https://mg-transport-content.s3.eu-central-1.amazonaws.com/whatsapp/1132e5dd8b263194dabd17d84074a7689380c79302d1a15615ead8678707fa2c.jpg',
                            messengerId: '79092182926',
                            extraData: [
                                ExtraDataProps::USERNAME => 'test',
                                ExtraDataProps::LAST_NAME => null,
                                ExtraDataProps::FIRST_NAME => '31414',
                            ],
                        ),
                        operator: $operator = new \BmPlatform\Abstraction\DataTypes\Operator(
                            externalId: '15627',
                            name: 'Мария Чистая',
                            extraData: [
                                ExtraDataProps::MAIN_API_ID => '18',
                                ExtraDataProps::FIRST_NAME => 'Мария',
                                ExtraDataProps::LAST_NAME => 'Чистая',
                                ExtraDataProps::ACCOUNT_TYPE => 'user',
                            ]
                        ),
                        messengerId: '79092182926',
                        extraData: [
                            ExtraDataProps::LAST_DIALOG_ID => 69106,
                        ]
                    ),
                    message: new MessageData(
                        externalId: '916857',
                        text: 'trans',
                        date: $date = Carbon::parse('2023-01-26T06:48:21.132332802Z'),
                        extraData: [
                            'order' => [
                                'number' => 'orderNumber',
                            ],
                            ExtraDataProps::EXTERNAL_MESSAGE_STATUS => 'sending',
                        ],
                    ),
                    operator: $operator,
                    timestamp: $date,
                ),
            ],

            [
                'message_new_outbox_channel',
                null,
            ],

            [
                'message_new_system',
                null,
            ]
        ];
    }
}
