<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\Abstraction\Events\ChatTransferredToOperator;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use Mockery as m;

class RDialogEventsHandlingTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testCloseDialogEvent()
    {

        \Illuminate\Support\Facades\Config::expects('get')->with('retailcrm.messenger_types.telegram.internal_type')->andReturn('telegram');
        $webhookDecodedData = json_decode(file_get_contents(__DIR__ . '/SampleData/WH/dialog_close.json'), true);

        $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $appHandler = m::mock(\BmPlatform\RetailCRM\AppHandler::class, [
            $u,
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [$appHandler, $webhookDecodedData]);

        $data->makePartial();

        $instance = m::mock(\BmPlatform\RetailCRM\EventHandlers\DialogClosed::class);
        $instance->makePartial();
        $this->assertEquals(
            new ChatTicketClosed(
                chat: new Chat(
                    externalId: 1,
                    messengerInstance: new \BmPlatform\Abstraction\DataTypes\MessengerInstance(
                        'telegram', 3, '@rcrmtestbot_bot',
                        messengerId: 'rcrmtestbot_bot',
                        internalType: \BmPlatform\Abstraction\Enums\MessengerType::Telegram
                    ),
                    contact: new Contact(
                        externalId: 5,
                        name: "Bogdan Shipilov",
                        avatarUrl: "https://mg-transport-content.s3.eu-central-1.amazonaws.com/telegram/a78cd8989ad9f7f6ac6a33cde689bae2e42cf5322b9302cf8dd43525c324ee03.jpg",
                        messengerId: '',
                        extraData: [
                            ExtraDataProps::USERNAME => 'theBodyan',
                            ExtraDataProps::FIRST_NAME => 'Bogdan',
                            ExtraDataProps::LAST_NAME => 'Shipilov',
                        ],
                    ),
                    operator: 8,
                    extraData: [ExtraDataProps::LAST_DIALOG_ID => 20]
                ),
                timestamp: $data->timestamp()
            ),
            $instance($data)
        );
    }

    public function testDialogAssignEvent()
    {
        \Illuminate\Support\Facades\Config::expects('get')->with('retailcrm.messenger_types.telegram.internal_type')->andReturn('telegram');

        $webhookDecodedData = json_decode(file_get_contents(__DIR__ . '/SampleData/WH/dialog_assign.json'), true);

        $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $appHandler = m::mock(\BmPlatform\RetailCRM\AppHandler::class, [
            $u,
            m::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $data = m::mock(\BmPlatform\RetailCRM\Utils\DataWrap::class, [$appHandler, $webhookDecodedData]);

        $data->makePartial();

        $instance = m::mock(\BmPlatform\RetailCRM\EventHandlers\DialogAssign::class);
        $instance->makePartial();

        $this->assertEquals(
            new ChatTransferredToOperator(
                chat: new Chat(
                    externalId: 1,
                    messengerInstance: new \BmPlatform\Abstraction\DataTypes\MessengerInstance(
                        'telegram', 3, '@rcrmtestbot_bot',
                        messengerId: 'rcrmtestbot_bot',
                        internalType: \BmPlatform\Abstraction\Enums\MessengerType::Telegram
                    ),
                    contact: new Contact(
                        externalId: 5,
                        name: "Bogdan Shipilov",
                        avatarUrl: "https://mg-transport-content.s3.eu-central-1.amazonaws.com/telegram/a78cd8989ad9f7f6ac6a33cde689bae2e42cf5322b9302cf8dd43525c324ee03.jpg",
                        messengerId: '123456',
                        extraData: [
                            ExtraDataProps::USERNAME => 'theBodyan',
                            ExtraDataProps::FIRST_NAME => 'Bogdan',
                            ExtraDataProps::LAST_NAME => 'Shipilov',
                        ],
                    ),
                    operator: 8,
                    messengerId: '123456',
                    extraData: [ExtraDataProps::LAST_DIALOG_ID => 20]
                ),
                newOperator: 8,
            ),
            $instance($data)
        );
    }
}
