<?php

use Illuminate\Support\Str;

class RImportsTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testPaginates()
    {
        $firstPage = json_decode(file_get_contents(__DIR__ . '/SampleData/API/pagination_page_1.json'), true);
        $secondPage = json_decode(file_get_contents(__DIR__ . '/SampleData/API/pagination_page_2.json'), true);

        $mainApiClient = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApiClient->expects('get')->with('get', ['query' => ['page' => 1, 'limit' => 20]])->andReturn(
            $firstPage
        )->once();
        $mainApiClient->expects('get')->with('get', ['query' => ['page' => 2, 'limit' => 20]])->andReturn(
            $secondPage
        )->once();
        $mainApiClient->makePartial();

        $expectedResult = (new \BmPlatform\Abstraction\DataTypes\IterableData(
            array_merge($firstPage['dataKeywordPlaceholder'], $secondPage['dataKeywordPlaceholder']), null
        ))->data;

        $cursor = 1;
        $result = [];
        while ($cursor) {
            $cursor = $mainApiClient->all('get', $cursor);
            $result = array_merge($result, $cursor->data['dataKeywordPlaceholder']);
            $cursor = $cursor->cursor;
        }

        $this->assertEquals($expectedResult, $result);
    }

    public function testDoesNotPaginate()
    {
        $botApiClient = Mockery::mock(\BmPlatform\RetailCRM\BotApiClient::class);
        $botApiClient->makePartial();

        try {
            $botApiClient->all('', 1);
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::FeatureNotSupported, $e->errorCode);
        }
    }

    public function testImportsOperatorGroups()
    {
        $apiOperatorGroups = json_decode(file_get_contents(__DIR__ . '/SampleData/API/operator_groups.json'), true);

        $mainApiClient = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApiClient->expects('get')->with('user-groups', ['query' => ['page' => 1, 'limit' => 20]])->andReturn(
            $apiOperatorGroups
        )->once();
        $mainApiClient->makePartial();

        $module = Mockery::mock(\BmPlatform\RetailCRM\AppHandler::class, [
            Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            Mockery::mock(\Illuminate\Contracts\Config\Repository::class),
        ]);
        $module->expects('getMainApiClient')->andReturn($mainApiClient)->once();
        $module->makePartial();

        $groups = $module->getOperatorGroups();

        $this->assertCount(3, $groups->data);
    }
}
