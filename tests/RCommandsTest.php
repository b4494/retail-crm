<?php


use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\MessengerInstance;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\RetailCRM\ApiClient;
use BmPlatform\Abstraction\Interfaces\Operator;
use BmPlatform\RetailCRM\MainApiClient;
use BmPlatform\RetailCRM\Utils\Entities\MessageEntity;
use \BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\RetailCRM\Commands;
use BmPlatform\RetailCRM\AppHandler;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\RetailCRM\Utils\Utils;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class RCommandsTest extends MockeryTestCase
{
    public function testSendsAvailableMessageTypes()
    {
        $messageSentSuccess = json_decode(
            '{
    "message_id": 309,
    "time": "2022-10-17T06:52:56.587663255Z"
}',
            true
        );

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn('1:3')->times(4);

        $appInstance = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $module = Mockery::mock(
            AppHandler::class,
            [
                $appInstance,
                Mockery::mock(Illuminate\Contracts\Config\Repository::class)
            ]
        );

        $botApiClient = Mockery::mock(\BmPlatform\RetailCRM\BotApiClient::class);

        $module->expects('getBotApiClient')->andReturn($botApiClient)->times(3);

        $commands = new Commands($module);

        $result = new MessageSendResult(
            MessageEntity::getExternalId([
                'id' => $messageSentSuccess['message_id'],
                'chat' => ['id' => '1']
            ])
        );

        // TEst text command
        $textRequest = new SendTextMessageRequest($chat, 'file.pdf');

        $botApiClient->expects('post')->with('messages', [
            'json' => [
                    'chat_id' => 1,
                    'scope' => 'public',
                    'type' => 'text',
                ] + ['content' => $textRequest->text]
        ])->andReturn($messageSentSuccess)->times(1);

        $this->assertEquals($result, $commands->sendTextMessage($textRequest));

        // Test media request
        $mediaRequest = new SendMediaRequest(
            $chat,
            new MediaFile(MediaFileType::Image, 'https://testhosting.com/file/image.png', name: 'name'),
            caption: 'caption',
        );

        $fileIdInSystem = 'file-id-hash';

        $botApiClient->expects('post')->with('files/upload_by_url', [
            'json' => ['url' => $mediaRequest->file->url]
        ])->andReturn(['id' => $fileIdInSystem])->times(1);

        $botApiClient->expects('post')->with('messages', [
            'json' => [
                    'chat_id' => 1,
                    'scope' => 'public',
                    'type' => 'image',
                    'note' => 'caption',
                ] + ['items' => [['id' => $fileIdInSystem, 'caption' => 'name']]]
        ])->andReturn($messageSentSuccess)->times(1);

        $this->assertEquals($result, $commands->sendMediaMessage($mediaRequest));
    }

    public function testTransfersToOperator()
    {
        $apiResponseSuccess = json_decode(
            file_get_contents(__DIR__ . '/SampleData/API/transfer_chat_success.json'),
            true
        );
        $apiResponseAlreadyTransferred = json_decode(
            file_get_contents(__DIR__ . '/SampleData/API/transfer_already_transferred.json'),
            true
        );
        $apiResponseOldDialogProvided = json_decode(
            file_get_contents(__DIR__ . '/SampleData/API/transfer_not_last_dialog.json'),
            true
        );
        $userId = 8;
        $dialogId = 25;

        $appInstance = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $module = Mockery::mock(
            AppHandler::class,
            [
                $appInstance,
                Mockery::mock(Illuminate\Contracts\Config\Repository::class)
            ]
        );
        $botApiClient = Mockery::mock(\BmPlatform\RetailCRM\BotApiClient::class);
        $module->expects('getBotApiClient')->andReturn($botApiClient)->zeroOrMoreTimes();

        $chat = Mockery::mock(Chat::class);

        $operator = Mockery::mock(Operator::class);
        $operator->expects('getExtraData')->andReturn([ExtraDataProps::ACCOUNT_TYPE => 'user'])->times(3);
        $operator->expects('getExternalId')->andReturn($userId)->times(3);

        $request = new TransferChatToOperatorRequest($chat, $operator);
        $internalResponse = new NewOperatorResponse($userId);

        $commands = new Commands($module);

        $chat->expects('getExternalId')->andReturn(123)->times(3);

        $botApiClient->expects('get')->with('dialogs', [ 'query' => [ 'chat_id' => 123, 'limit' => 1 ]])->andReturn([
            [ 'responsible' => null, 'id' => $dialogId ]
        ])->times(3);

        $botApiClient->expects('patch')->with('dialogs/:id/assign', [
            'params' => ['id' => $dialogId],
            // TODO: do we treat other bots as operators too?
            'json' => [
                'user_id' => $userId
            ]
        ])->andReturn($apiResponseSuccess)->times(1);

        $this->assertEquals($internalResponse, $commands->transferChatToOperator($request));

        $botApiClient->expects('patch')->with('dialogs/:id/assign', [
            'params' => ['id' => $dialogId],
            // TODO: do we treat other bots as operators too?
            'json' => [
                'user_id' => $userId
            ]
        ])->andThrow(new ErrorException(ErrorCode::BadRequest, $apiResponseAlreadyTransferred['errors'][0]))->times(1);

        $commands = new Commands($module);
        try {
            $commands->transferChatToOperator($request);
        }
        catch (ErrorException $e) {
            $this->assertEquals(ErrorCode::OperatorAlreadyAssigned, $e->errorCode);
        }

        $botApiClient->expects('patch')->with('dialogs/:id/assign', [
            'params' => ['id' => $dialogId],
            // TODO: do we treat other bots as operators too?
            'json' => [
                'user_id' => $userId
            ]
        ])->andThrow(new ErrorException(ErrorCode::BadRequest, $apiResponseOldDialogProvided['errors'][0]))->times(1);

        $commands = new Commands($module);
        try {
            $commands->transferChatToOperator($request);
        } catch (ErrorException $e) {
            $this->assertEquals(ErrorCode::BadRequest, $e->errorCode);
        }
    }

    public function testChatTicketCloses()
    {
        $apiResponseAlreadyClosed = json_decode(
            file_get_contents(__DIR__ . '/SampleData/API/dialog_already_closed.json'),
            true
        );

        $dialogId = 21;
        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn(123);

        $appInstance = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $module = Mockery::mock(
            AppHandler::class,
            [
                $appInstance,
                Mockery::mock(Illuminate\Contracts\Config\Repository::class)
            ]
        );
        $botApiClient = Mockery::mock(\BmPlatform\RetailCRM\BotApiClient::class);
        $module->expects('getBotApiClient')->andReturn($botApiClient)->zeroOrMoreTimes();

        $botApiClient->expects('get')->with('dialogs', [ 'query' => [ 'chat_id' => 123, 'limit' => 1 ] ])
            ->andReturn([[
              'is_active' => true,
              'id' => $dialogId,
          ]]);
        $botApiClient->expects('delete')->with(
            'dialogs/:id/close',
            ['params' => ['id' => $dialogId]]
        );
        $commands = new Commands($module);
        $commands->closeChatTicket($chat);
    }

    public function testChatTicketClosed()
    {
        $apiResponseAlreadyClosed = json_decode(
            file_get_contents(__DIR__ . '/SampleData/API/dialog_already_closed.json'),
            true
        );

        $dialogId = 21;
        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn(123);

        $appInstance = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $module = Mockery::mock(
            AppHandler::class,
            [
                $appInstance,
                Mockery::mock(Illuminate\Contracts\Config\Repository::class)
            ]
        );
        $botApiClient = Mockery::mock(\BmPlatform\RetailCRM\BotApiClient::class);
        $module->expects('getBotApiClient')->andReturn($botApiClient);

        $botApiClient->expects('get')->with('dialogs', [ 'query' => [ 'chat_id' => 123, 'limit' => 1 ] ])->andReturn([[
            'is_active' => false,
        ]]);

//        $botApiClient->expects('delete')->with(
//            'dialogs/:id/close',
//            ['params' => ['id' => $dialogId]]
//        )->andThrow(new ErrorException(ErrorCode::BadRequest, $apiResponseAlreadyClosed['errors'][0]))->times(1);

        $commands = new Commands($module);

        try {
            $commands->closeChatTicket($chat);
        } catch (ErrorException $e){
            $this->assertEquals(ErrorCode::ChatTicketAlreadyClosed, $e->errorCode);
        }
    }

    public function testCustomerUpdates(): void
    {
        $apiClient = Mockery::mock(MainApiClient::class);

        $contact = Mockery::mock(Contact::class);

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getMainApiClient')->andReturn($apiClient)->zeroOrMoreTimes();
        $module->expects('resolveCustomer')->with($contact)->andReturn([ 'site' => 'site', 'id' => 123 ]);

        $commands = new BmPlatform\RetailCRM\Commands($module);

        $apiClient->expects('post')->withArgs(function ($path, $options) {
            if ($path == 'customers/:id/edit') {
                $this->assertEquals([
                    'params' => [ 'id' => 123 ],
                    'json' => [
                        'by' => 'id', 'site' => 'site', 'customer' => json_encode([
                            'phones' => [ [ 'number' => 'phone' ] ],
                            'name' => 'name',
                            'birthday' => '2024-11-14',
                            'customFields' => [
                                'chat_hash' => 'hash',
                            ]
                        ])
                    ]
                ], $options);

                return true;
            } elseif ($path == 'customers/notes/create') {
                $this->assertEquals([
                    'json' => [ 'site' => 'site', 'note' => json_encode([
                        'customer' => [ 'id' => 123 ],
                        'text' => 'note',
                ])]], $options);

                return true;
            } else {
                return false;
            }
        })->andReturnNull()->times(2);

        $commands->updateContactData(new UpdateContactDataRequest($contact, [
            'phone' => 'phone',
            'name' => 'name',
            'birthday' => \Carbon\Carbon::parse('2024-11-14'),
            'addNote' => 'note',
            'CF_chat_hash' => 'hash',
        ]));
    }
}
