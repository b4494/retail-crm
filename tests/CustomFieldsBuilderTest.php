<?php

class CustomFieldsBuilderTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testCustomFieldsGenerator(): void
    {
        $apiClient = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $apiClient->expects('all')->with('custom-fields/dictionaries', 1)->andReturn(new \BmPlatform\Abstraction\DataTypes\IterableData([
            'customDictionaries' => [
                [
                    'code' => 'dict',
                    'elements' => [
                        ['name' => 'Value A', 'code' => 'a' ],
                        ['name' => 'Value B', 'code' => 'b' ],
                    ]
                ]
            ]
        ], null));

        $apiClient->expects('all')->with('custom-fields', 1, [
            'query' => [ 'filter' => [ 'entity' => 'customer' ]],
        ])->andReturn(new \BmPlatform\Abstraction\DataTypes\IterableData([
            'customFields' => [
                ['name' => 'Field 1', 'code' => 'field1', 'type' => 'string'],
                ['name' => 'Field 2', 'code' => 'field2', 'type' => 'dictionary', 'dictionary' => 'dict'],
                ['name' => 'Field 3', 'code' => 'field3', 'type' => 'integer'],
            ]
        ], null));

        $builder = new \BmPlatform\RetailCRM\CustomFieldsBuilder($apiClient);
        $res = $builder();
        $this->assertEquals([
            'field1' => [ 'Field 1', 'string' ],
            'field3' => [ 'Field 3', 'int' ],
            'field2' => [ 'Field 2', 'enum', ['options' => [
                ['value' => 'a','label' => 'Value A'],
                ['value' => 'b','label' => 'Value B'],
            ]] ],
        ], $res);
    }
}