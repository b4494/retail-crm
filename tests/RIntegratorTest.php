<?php

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Abstraction\Enums\PaymentType;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;

class RIntegratorTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testIntegrates()
    {
        $botCode = 'bot-marketing-bot';
        $botName = 'bot-marketing Bot';
        $publicUrl = 'https://public.retailcrm.com';
        $systemUrl = 'https://system.retailcrm.com';
        $retailCrmApiKey = 'apikey';
        $email = 'testemail@gmail.com';
        $name = 'testname';

        $botResponse = '{
    "success": true,
    "info": {
        "mgBot": {
            "endpointUrl": "https://botmarketing.local",
            "token": "18528615asdasdasd56f967zzzzf19LLLL39e05QWERREWEEEEQTQRQW0c1w22126ebw1a"
        }
    }
}';

        $botFailureResponse = '{
    "success": true,
    "info": {
        "mgBot": {
            "endpointUrl": "https://botmarketing.local"
        }
    }
}';

        $usersResponse = '{
    "success": true,
    "pagination": {
        "limit": 20,
        "totalCount": 2,
        "currentPage": 1,
        "totalPageCount": 1
    },
    "users": [
        {
            "id": 14,
            "createdAt": "2022-10-02 06:28:27",
            "active": false,
            "email": "mail@retailcrm.ru",
            "firstName": "Техническая",
            "lastName": "Поддержка",
            "status": "free",
            "online": false,
            "isAdmin": true,
            "isManager": true,
            "groups": [
                {
                    "id": 5,
                    "name": "Администраторы",
                    "code": "superadmins"
                },
                {
                    "id": 4,
                    "name": "Руководители",
                    "code": "director"
                },
                {
                    "id": 3,
                    "name": "Менеджеры",
                    "code": "manager"
                }
            ]
        },
        {
            "id": 13,
            "createdAt": "2022-10-02 06:28:26",
            "active": true,
            "email": "' . $email . '",
            "firstName": "' . $name . '",
            "status": "free",
            "online": true,
            "isAdmin": true,
            "isManager": true,
            "groups": [
                {
                    "id": 5,
                    "name": "Администраторы",
                    "code": "superadmins"
                },
                {
                    "id": 4,
                    "name": "Руководители",
                    "code": "director"
                },
                {
                    "id": 3,
                    "name": "Менеджеры",
                    "code": "manager"
                }
            ],
            "mgUserId": 1
        }
    ]
}';
        $token = 'token';
        $integrator = Mockery::mock(\BmPlatform\RetailCRM\Integrator::class, [
            [
                'systemUrl' => $systemUrl,
                'publicUrl' => $publicUrl,
                'apiKey' => $retailCrmApiKey,
            ],
            false,
        ]);
        $integrator->shouldAllowMockingProtectedMethods();
        $integrator->expects('createMainApiClient')->andReturn(
            $mainApiClient = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class)
        )->times(2);

        $integrator->makePartial();

        Config::expects('get')->with('retailcrm.applications.main')->andReturn([
            'code' => $botCode,
            'botName' => $botName,
        ]);
        \Illuminate\Support\Facades\Log::expects('warning')->withAnyArgs()->times(1);
        // in real world, this case is only possible when active is set to false.
        $callbackId = 'callback';
        $mockedUrl = 'https://thisboturl.com';
        $usersResponseDecoded = json_decode($usersResponse, true)['users'][0];
        Config::expects('get')->with('retailcrm_params.required')->andReturn([])->times(4);
        $integrator->expects('initializeBot')->with($mainApiClient, $botCode, $botName, $callbackId)->andReturn(
            json_decode($botResponse, true)
        )->once();

        $integrator->expects('getOldestAdminUser')->withAnyArgs()->andReturn(['email' => $email, 'firstName' => $name])->times(2);

        $this->assertEquals(
            new \BmPlatform\Abstraction\DataTypes\AppIntegrationData(
                type: 'retailcrm',
                externalId: \BmPlatform\RetailCRM\Utils\Utils::extractSubdomain($systemUrl),
                name: 'public.retailcrm.com',
                email: $email,
                extraData: [
                    ExtraDataProps::MG_BOT_API_URL => 'https://botmarketing.local',
                    ExtraDataProps::MG_BOT_API_TOKEN => '18528615asdasdasd56f967zzzzf19LLLL39e05QWERREWEEEEQTQRQW0c1w22126ebw1a',
                ],
                paymentType: PaymentType::External,
                status: new AppExternalStatus(AppStatus::Active, PaymentStatus::Paid),
                accountType: \BmPlatform\Abstraction\Enums\AccountType::Full,
            ),
            $integrator->getCompleteIntegrationInstance($callbackId)
        );

        $integrator->expects('initializeBot')->with($mainApiClient, $botCode, $botName, $callbackId)->andReturn(
            json_decode($botFailureResponse, true)
        )->once();
        try {
            $integrator->getCompleteIntegrationInstance($callbackId);
            $this->fail('Should throw exception');
        } catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::IntegrationNotPossible, $e->errorCode);
        }
    }
}
