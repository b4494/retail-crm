<?php

class ExtraDataTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testShortRepeatsAbsentInsideClasses()
    {
        $reflection = new ReflectionClass(\BmPlatform\RetailCRM\Utils\ExtraDataProps::class);
        $constants = $reflection->getConstants();
        $repeatedShortKeys = $this->getRepeatedKeys($constants);
        $this->assertEmpty($repeatedShortKeys, 'The following keys are repeated: ' . implode(', ', $repeatedShortKeys));
    }

    // don't pass nested arrays.
    protected function getRepeatedKeys(array $array)
    {
        $repeatedKeys = [];
        foreach (array_count_values($array) as $val => $count) {
            if ($count > 1) {
                $repeatedKeys[] = $val;
            }
        }
        return $repeatedKeys;
    }
}
