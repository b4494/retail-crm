<?php

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class RPaymentStatusTest extends MockeryTestCase
{
    protected bool $frozen = true;
    protected bool $active = true;

    protected function setUp(): void
    {
        \Illuminate\Support\Facades\Config::expects('get')->with('retailcrm.applications.main.code')->andReturn('code');
    }

    public function testInternalActive()
    {
        $mainApi = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApi->expects('get')->withAnyArgs()->andReturn(
            json_decode(
                file_get_contents(__DIR__ . '/SampleData/API/integration_module_active.json'),
                true
            )
        )->once();

        $this->assertEquals(
            new AppExternalStatus(AppStatus::Active, PaymentStatus::Paid),
            $this->getHandler($mainApi)->status(),
        );
    }

    public function testInternalDisabled()
    {
        $mainApi = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApi->expects('get')->withAnyArgs()->andReturn(
            json_decode(
                file_get_contents(__DIR__ . '/SampleData/API/integration_module_disabled.json'),
                true
            )
        )->once();

        $this->assertEquals(new AppExternalStatus(AppStatus::Disabled), $this->getHandler($mainApi)->status());

        $mainApi->expects('get')->withAnyArgs()->andThrow(
            new ErrorException(ErrorCode::UnexpectedServerError, '')
        )->once();

        $this->expectException(ErrorException::class);

        $this->getHandler($mainApi)->status();

        $mainApi->expects('get')->withAnyArgs()->andThrow(
            new ErrorException(ErrorCode::InsufficientFunds, '')
        )->once();

        $this->assertEquals(new AppExternalStatus(AppStatus::Frozen), $this->getHandler($mainApi)->status());
    }

    public function testInternalFrozen()
    {
        $mainApi = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApi->expects('get')->withAnyArgs()->andReturn(
            json_decode(
                file_get_contents(__DIR__ . '/SampleData/API/integration_module_frozen.json'),
                true
            )
        )->once();

        $this->assertEquals(new AppExternalStatus(AppStatus::Frozen), $this->getHandler($mainApi)->status());
    }

    public function testExternalActive()
    {
        $mainApi = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApi->expects('get')->withAnyArgs()->andReturn(
            json_decode(
                file_get_contents(__DIR__ . '/SampleData/API/integration_module_active.json'),
                true
            )
        )->once();

        $this->assertEquals(new AppExternalStatus(AppStatus::Active, PaymentStatus::Paid), $this->getHandler($mainApi)->status());
    }

    public function testExternalDisabled()
    {
        $appStatus = new AppExternalStatus(
            AppStatus::Disabled
        );

        $mainApi = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApi->expects('get')->withAnyArgs()->andReturn(
            json_decode(
                file_get_contents(__DIR__ . '/SampleData/API/integration_module_disabled.json'),
                true
            )
        )->once();

        $this->assertEquals($appStatus, $this->getHandler($mainApi)->status());
    }

    public function testExternalUnpaid()
    {
        $appStatus = new AppExternalStatus(
            AppStatus::Frozen
        );

        $mainApi = Mockery::mock(\BmPlatform\RetailCRM\MainApiClient::class);
        $mainApi->expects('get')->withAnyArgs()->andReturn(
            json_decode(
                file_get_contents(__DIR__ . '/SampleData/API/integration_module_frozen.json'),
                true
            )
        )->once();

        $this->assertEquals($appStatus, $this->getHandler($mainApi)->status());

        $mainApi->expects('get')->withAnyArgs()->andThrow(
            new ErrorException(ErrorCode::InsufficientFunds, '')
        )->once();

        $this->assertEquals($appStatus, $this->getHandler($mainApi)->status());
    }

    public function getHandler($mainApiMock)
    {
        $appInstance = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
        $appInstance->expects('getExternalId')->andReturn('test')->zeroOrMoreTimes();
        $handler = Mockery::mock(
            \BmPlatform\RetailCRM\AppHandler::class,
            [$appInstance, Mockery::mock(Illuminate\Contracts\Config\Repository::class), $mainApiMock]
        );
        $handler->makePartial();
        \Illuminate\Support\Facades\Config::expects('get')->with('retailcrm_params.bot_code')->andReturn(
            ''
        )->zeroOrMoreTimes();
        \Illuminate\Support\Facades\Config::expects('get')->with('retailcrm_params.required')->andReturn([]
        )->zeroOrMoreTimes();
        \Illuminate\Support\Facades\Log::expects('warning')->withAnyArgs()->zeroOrMoreTimes();
        return $handler;
    }
}
