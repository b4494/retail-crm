<?php

namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use GuzzleHttp\RequestOptions;

class BotApiClient extends MainApiClient
{
    public function all(string $path, int $page = 1, array $queryParams = ['query' => []], int $limit = 20): IterableData
    {
        throw new ErrorException(ErrorCode::FeatureNotSupported, 'Pagination is not supported in MG Bot api. Only limiting.');
    }

    protected function defaultConfig(): array
    {
        return [
            'base_uri' => $this->domain . '/api/bot/v1/',
            RequestOptions::HEADERS => [
                'x-bot-token' => $this->token,
                'Accept' => 'application/json',
            ],
            RequestOptions::CONNECT_TIMEOUT => 3,
            RequestOptions::TIMEOUT => 30,
        ];
    }
}
