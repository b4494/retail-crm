<?php

namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Exceptions\ValidationException;
use BmPlatform\Support\Http\HttpClient;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;

class MainApiClient extends HttpClient
{
    public function __construct(public string $domain, public string $token, array $options = [])
    {
        parent::__construct($options);
    }

    public function all(string $path, int $page = 1, array $queryParams = ['query' => []], int $limit = 20): IterableData
    {
        $validLimits = [100, 50, 20];
        if (!in_array($limit, $validLimits)) {
            foreach ($validLimits as $l) {
                if ($limit > $l) {
                    $limit = $l;
                    break;
                }
            }
        }
        $options = $queryParams;
        $options['query']['page'] = $page;
        $options['query']['limit'] = $limit;
        $response = $this->get($path, $options);
        if (data_get($response, 'pagination.totalPageCount', $page) > $page) {
            return new IterableData($response, $page + 1);
        }
        return new IterableData($response, null);
    }

    protected function defaultConfig(): array
    {
        return [
            'base_uri' => $this->getDomain(),
            RequestOptions::HEADERS => [
                'x-api-key' => $this->token,
                'Accept' => 'application/json',
                'X-BotMarketing' => 'SomeTokenThere',
            ],
            RequestOptions::CONNECT_TIMEOUT => 3,
            RequestOptions::TIMEOUT => 60,
        ];
    }

    protected function getDomain(): string
    {
        Str::endsWith($this->domain, '/') ?: $this->domain .= '/';
        return $this->domain . 'api/v5/';
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    protected function processResponse(ResponseInterface $response): mixed
    {
        if (500 <= $code = $response->getStatusCode()) {
            throw $this->handleServerError($code);
        }
        if ($data = $response->getBody()->getContents()) {
            return $this->parseData($data, $code);
        } else {
            return $this->handleEmptyResponse($code);
        }
    }

    private function handleServerError(int $code): ErrorException
    {
        return match ($code) {
            503 => throw new ErrorException(ErrorCode::TooManyRequests, 'Too many requests, try again.'),
            default => new ErrorException(ErrorCode::InternalServerError, $code),
        };
    }

    private function handleEmptyResponse(int $code): bool
    {
        return match (true) {
            $code < 400 => true,
            default => throw new ErrorException(ErrorCode::UnexpectedServerError, $code),
        };
    }

    private function parseData(string $data, int $code)
    {
        try {
            $data = json_decode($data, true, 512, JSON_INVALID_UTF8_IGNORE | JSON_THROW_ON_ERROR);
        } catch (\JsonException) {
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Failed to parse json');
        }

        if ($code < 400) {
            return $data ?? true;
        }

        $errors = $data['errorMsg'] ?? $data['errors'] ?? null;
        $errorMessage = is_string($errors) ? $errors : $this->getErrorOrUnknown($errors);

        throw match ($code) {
            404 => throw new ErrorException(ErrorCode::NotFound, $errorMessage),
            403 => throw new ErrorException(match ($errorMessage) {
                'channel deactivated' => ErrorCode::MessengerInstanceDisabled,
                default => ErrorCode::AuthenticationFailed,
            }, $errorMessage),
            402 => throw new ErrorException(ErrorCode::InsufficientFunds, $errorMessage),
            400 => throw new ErrorException(ErrorCode::BadRequest, $errorMessage),
            default => throw new ErrorException(ErrorCode::UnexpectedServerError, $errorMessage)
        };
    }

    private function getErrorOrUnknown(mixed $errors = [])
    {
        return $errors[0] ?? 'Unknown error';
    }
}
