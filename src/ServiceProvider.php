<?php

namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Abstraction\Interfaces\Operator;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Interfaces\VariableRegistrar;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use Illuminate\Support\Carbon;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const TR_PREFIX = 'retailcrm::fields.';

    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/retailcrm.php', 'retailcrm');
        $this->mergeConfigFrom(__DIR__ . '/../config/retailcrm_params.php', 'retailcrm_params');
        $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');
        $this->loadViewsFrom(__DIR__ . '/Resources/Views', 'retailcrm');
        $this->loadTranslationsFrom(__DIR__ . '/../lang', 'retailcrm');

        $config = $this->app['config'];

        $this->makeHub()->registerAppType('retailcrm', static fn(AppInstance $app) => new AppHandler($app, $config), [
            'schema' => static fn() => $config->get('retailcrm'),
            'registerVariables' => $this->registerVariables(),
            'registerInstanceVariables' => $this->registerInstanceVariables(),
        ]);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makeHub(): Hub
    {
        return $this->app->make(Hub::class);
    }

    private function registerVariables()
    {
        return static function (VariableRegistrar $registrar) {
            $registrar->complex('integration', static function (VariableRegistrar $registrar) {
                $registrar->text('apiDomain', static fn (array $data) => $data[ExtraDataProps::MAIN_API_URL] ?? null);
                $registrar->text('apiKey', static fn (array $data) => $data[ExtraDataProps::MAIN_API_KEY] ?? null);
                $registrar->text('botApiUrl', static fn (array $data) => $data[ExtraDataProps::MG_BOT_API_URL] ?? null);
                $registrar->text('botApiToken', static fn (array $data) => $data[ExtraDataProps::MG_BOT_API_TOKEN] ?? null);
            }, static fn (RuntimeContext $context) => $context->appInstance()->getExtraData())
                ->name(trans(self::TR_PREFIX.'integration'));

            $registrar->enum('operator.type', [
                [ 'value' => 'user', 'label' => trans(self::TR_PREFIX.'account_type_user') ],
                [ 'value' => 'bot', 'label' => trans(self::TR_PREFIX.'account_type_bot') ],
            ], static fn (Operator $v) => $v->getExtraData()[ExtraDataProps::ACCOUNT_TYPE])
                ->name(trans(self::TR_PREFIX.'account_type'));

            $registrar->text('operator.firstName', static fn (Operator $v) => $v->getExtraData()[ExtraDataProps::FIRST_NAME] ?? null);
            $registrar->text('operator.lastName', static fn (Operator $v) => $v->getExtraData()[ExtraDataProps::LAST_NAME] ?? null);

            $registrar->text('contact.username', static fn (Contact $v) => $v->getExtraData()[ExtraDataProps::USERNAME] ?? null);

            $registrar->complex('contact.customer', function (VariableRegistrar $registrar) {
                $registrar->text('type')->name(trans(self::TR_PREFIX.'type'));
                $registrar->int('id')->name(trans(self::TR_PREFIX.'id'));
                $registrar->text('externalId')->name(trans(self::TR_PREFIX.'externalId'));
                $registrar->bool('isContact')->name(trans(self::TR_PREFIX.'isContact'));

                $registrar->datetime(
                    'createdAt',
                    static fn ($v, RuntimeContext $ctx) => Carbon::parse($v['createdAt'], $ctx->appInstance()->getTimeZone())
                )->name(trans(self::TR_PREFIX.'createdAt'));

                $registrar->int('managerId')->name(trans(self::TR_PREFIX.'managerId'));
                $registrar->bool('vip')->name(trans(self::TR_PREFIX.'vip'));
                $registrar->bool('bad')->name(trans(self::TR_PREFIX.'bad'));
                $registrar->text('site')->name(trans(self::TR_PREFIX.'site'));
                $registrar->text('firstClientId')->name(trans(self::TR_PREFIX.'firstClientId'));
                $registrar->text('lastClientId')->name(trans(self::TR_PREFIX.'lastClientId'));
                $registrar->float('avgMarginSumm')->name(trans(self::TR_PREFIX.'avgMarginSumm'));
                $registrar->float('marginSumm')->name(trans(self::TR_PREFIX.'marginSumm'));
                $registrar->float('totalSumm')->name(trans(self::TR_PREFIX.'totalSumm'));
                $registrar->float('averageSumm')->name(trans(self::TR_PREFIX.'averageSumm'));
                $registrar->int('ordersCount')->name(trans(self::TR_PREFIX.'ordersCount'));
                $registrar->float('costSumm')->name(trans(self::TR_PREFIX.'costSumm'));
                $registrar->float('personalDiscount')->name(trans(self::TR_PREFIX.'personalDiscount'));
                $registrar->float('cumulativeDiscount')->name(trans(self::TR_PREFIX.'cumulativeDiscount'));
                $registrar->text('discountCardNumber')->name(trans(self::TR_PREFIX.'discountCardNumber'));
                $registrar->int('maturationTime')->name(trans(self::TR_PREFIX.'maturationTime'));
                $registrar->text('firstName')->name(trans(self::TR_PREFIX.'firstName'));
                $registrar->text('lastName')->name(trans(self::TR_PREFIX.'lastName'));
                $registrar->text('patronymic')->name(trans(self::TR_PREFIX.'patronymic'));
                $registrar->text('sex')->name(trans(self::TR_PREFIX.'sex'));
                $registrar->text('presumableSex')->name(trans(self::TR_PREFIX.'presumableSex'));
                $registrar->text('email')->name(trans(self::TR_PREFIX.'email'));

                $registrar->datetime('emailMarketingUnsubscribedAt', static fn ($v, RuntimeContext $ctx) => isset($v['emailMarketingUnsubscribedAt'])
                    ? Carbon::parse($v['emailMarketingUnsubscribedAt'], $ctx->appInstance()->getTimezone())
                    : null
                )->name(trans(self::TR_PREFIX.'emailMarketingUnsubscribedAt'));

                $registrar->date('birthday', static fn ($v, RuntimeContext $ctx) => Carbon::parse($v['birthday'], $ctx->appInstance()->getTimezone()))
                    ->name(trans(self::TR_PREFIX.'birthday'));

                $registrar->text('photoUrl')->name(trans(self::TR_PREFIX.'photoUrl'));

                $registrar->complex('phones', static function (VariableRegistrar $registrar) {
                    $registrar->text('number')->name(trans(self::TR_PREFIX.'phones.number'));
                })->name(trans(self::TR_PREFIX.'phones'))->array();

                $registrar->complex('contragent', static function (VariableRegistrar $registrar) {
                        $registrar->text('contragentType')->name(trans(self::TR_PREFIX.'contragent.contragentType'));
                        $registrar->text('legalName')->name(trans(self::TR_PREFIX.'contragent.legalName'));
                        $registrar->text('legalAddress')->name(trans(self::TR_PREFIX.'contragent.legalAddress'));
                        $registrar->text('INN')->name(trans(self::TR_PREFIX.'contragent.INN'));
                        $registrar->text('OKPO')->name(trans(self::TR_PREFIX.'contragent.OKPO'));
                        $registrar->text('KPP')->name(trans(self::TR_PREFIX.'contragent.KPP'));
                        $registrar->text('OGRN')->name(trans(self::TR_PREFIX.'contragent.OGRN'));
                        $registrar->text('OGRNIP')->name(trans(self::TR_PREFIX.'contragent.OGRNIP'));
                        $registrar->text('certificateNumber')->name(trans(self::TR_PREFIX.'contragent.certificateNumber'));
                        $registrar->datetime('certificateDate')->name(trans(self::TR_PREFIX.'contragent.certificateDate'));
                        $registrar->text('BIK')->name(trans(self::TR_PREFIX.'contragent.BIK'));
                        $registrar->text('bank')->name(trans(self::TR_PREFIX.'contragent.bank'));
                        $registrar->text('bankAddress')->name(trans(self::TR_PREFIX.'contragent.bankAddress'));
                        $registrar->text('corrAccount')->name(trans(self::TR_PREFIX.'contragent.corrAccount'));
                        $registrar->text('bankAccount')->name(trans(self::TR_PREFIX.'contragent.bankAccount'));
                    }
                )->name(trans(self::TR_PREFIX.'contragent'));

                $registrar->complex('address', static function (VariableRegistrar $registrar) {
                    $registrar->int('id')->name(trans(self::TR_PREFIX.'address.id'));
                    $registrar->text('index')->name(trans(self::TR_PREFIX.'address.index'));
                    $registrar->text('countryIso')->name(trans(self::TR_PREFIX.'address.countryIso'));
                    $registrar->text('region')->name(trans(self::TR_PREFIX.'address.region'));
                    $registrar->int('regionId')->name(trans(self::TR_PREFIX.'address.regionId'));
                    $registrar->text('city')->name(trans(self::TR_PREFIX.'address.city'));
                    $registrar->int('cityId')->name(trans(self::TR_PREFIX.'address.cityId'));
                    $registrar->text('cityType')->name(trans(self::TR_PREFIX.'address.cityType'));
                    $registrar->text('street')->name(trans(self::TR_PREFIX.'address.street'));
                    $registrar->int('streetId')->name(trans(self::TR_PREFIX.'address.streetId'));
                    $registrar->text('streetType')->name(trans(self::TR_PREFIX.'address.streetType'));
                    $registrar->text('building')->name(trans(self::TR_PREFIX.'address.building'));
                    $registrar->text('flat')->name(trans(self::TR_PREFIX.'address.flat'));
                    $registrar->int('floor')->name(trans(self::TR_PREFIX.'address.floor'));
                    $registrar->int('block')->name(trans(self::TR_PREFIX.'address.block'));
                    $registrar->text('house')->name(trans(self::TR_PREFIX.'address.house'));
                    $registrar->text('housing')->name(trans(self::TR_PREFIX.'address.housing'));
                    $registrar->text('metro')->name(trans(self::TR_PREFIX.'address.metro'));
                    $registrar->text('notes')->name(trans(self::TR_PREFIX.'address.notes'));
                    $registrar->text('text')->name(trans(self::TR_PREFIX.'address.text'));
                    $registrar->text('externalId')->name(trans(self::TR_PREFIX.'address.externalId'));
                    $registrar->text('name')->name(trans(self::TR_PREFIX.'address.name'));
                }
                )->name(trans(self::TR_PREFIX.'address'));

                $registrar->complex('segments', static function (VariableRegistrar $registrar) {
                    $registrar->int('id');
                    $registrar->text('code');

                    $registrar->text('name');
                    $registrar->datetime('createdAt', static fn ($v, RuntimeContext $ctx) => Carbon::parse($v['createdAt'], $ctx->appInstance()->getTimezone()));

                    $registrar->bool('isDynamic')->name(trans(self::TR_PREFIX.'segments.isDynamic'));
                    $registrar->int('customersCount')->name(trans(self::TR_PREFIX.'segments.customersCount'));
                    $registrar->bool('active')->name(trans(self::TR_PREFIX.'segments.active'));
                })->name(trans(self::TR_PREFIX.'segments'))->array();

                $registrar->complex('source', static function (VariableRegistrar $registrar) {
                    $registrar->text('source')->name(trans(self::TR_PREFIX.'source.source'));
                    $registrar->text('medium')->name(trans(self::TR_PREFIX.'source.medium'));
                    $registrar->text('campaign')->name(trans(self::TR_PREFIX.'source.campaign'));
                    $registrar->text('keyword')->name(trans(self::TR_PREFIX.'source.keyword'));
                    $registrar->text('content')->name(trans(self::TR_PREFIX.'source.content'));
                })->name(trans(self::TR_PREFIX.'source'));
            }, static function (Contact $contact, RuntimeContext $ctx) {
                return $ctx->cacheValue(
                    'customer',
                    static fn () => $ctx->appInstance()->getHandler()->resolveCustomer($contact),
                );
            })->name(trans(self::TR_PREFIX.'customer'));

            $registrar->text('contact.firstName', static fn (Contact $c) => $c->getExtraData()[ExtraDataProps::FIRST_NAME]);
            $registrar->text('contact.lastName', static fn (Contact $c) => $c->getExtraData()[ExtraDataProps::LAST_NAME]);
        };
    }

    private function registerInstanceVariables(): callable
    {
        return function (VariableRegistrar $registrar, AppInstance $appInstance) {
            $registrar->complex('contact.customer.customFields', function (VariableRegistrar $registrar) use ($appInstance) {
                $td = $appInstance->getTemporaryData();

                if (!is_array($td->data)) return;

                foreach ($td->data['cf'] as $key => $config) {
                    (match ($config[1]) {
                        'text', 'string', 'email' => $registrar->text($key),
                        'date' => $registrar->date($key),
                        'datetime' => $registrar->dateTime($key),
                        'time' => $registrar->time($key),
                        'boolean' => $registrar->bool($key),
                        'int' => $registrar->int($key),
                        'number' => $registrar->number($key),
                        'enum' => $registrar->enum($key, $config[2]['options'])->array($config[2]['multiple'] ?? false),
                    })->name($config[0]);
                }
            })->name(trans(self::TR_PREFIX.'customFields'));
        };
    }
}

