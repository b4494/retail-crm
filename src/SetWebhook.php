<?php


namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\RetailCRM\Utils\Utils;
use BmPlatform\Support\Http\HttpClient;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Psr\Http\Message\ResponseInterface;

class SetWebhook
{
    const EVENTS = [
        'chat_updated',
        'dialog_assign',
        'dialog_closed',
        'message_new',
        'message_updated',
    ];

    public function __construct(public readonly AppHandler $handler, public readonly Hub $hub)
    {
        //
    }

    public function __invoke($enable = true)
    {
        if (App::isLocal()) {
            return;
        }
        $httpClient = new class extends HttpClient {
            protected function defaultConfig(): array
            {
                return [
                    'base_uri' => Utils::getRetailCrmParam('ws_address'),
                ];
            }

            protected function processResponse(ResponseInterface $response): mixed
            {
                $contents = json_decode($response->getBody()->getContents(), true);
                $code = $response->getStatusCode();
                if ($code == 200) {
                    return $contents;
                }
                return match ($code) {
                    404 => throw new ErrorException(ErrorCode::NotFound, 'WS server could not find the specified connection in storage.'),
                    400 => throw new ErrorException(ErrorCode::BadRequest, $contents['message'] ?? 'Unexpected WS server error'),
                    default => throw new ErrorException(ErrorCode::IntegrationNotPossible, 'Unexpected WS server error.'),
                };
            }
        };

        $callbackUrl = $this->getCallbackUrl();

        $payload = [
            'host' => parse_url($this->handler->user->getExtraData()[ExtraDataProps::MG_BOT_API_URL])['host'],
            'token' => $this->handler->user->getExtraData()[ExtraDataProps::MG_BOT_API_TOKEN],
            'apiVersion' => 'v1',
            'webhookUrl' => $callbackUrl,
            'domain' => $this->handler->user->getExternalId(),
        ];

        if (!$enable) {
            try {
                $httpClient->put('remove', ['json' => $payload]);
            } catch (ErrorException $e) {
                if ($e->errorCode === ErrorCode::NotFound) {
                    return;
                }

                Log::error('WS server error: '.$e->getMessage(), [
                    'appInstance' => $this->handler->user,
                    'payload' => $payload,
                    'errorCode' => $e->errorCode
                ]);

                throw new ErrorException(ErrorCode::InternalServerError, $e->getMessage());
            }

            return;
        }

        try {
            $subscribed = $httpClient->post('add', ['json' => $payload]);
        } catch (ErrorException $e) {
            if ($e->errorCode === ErrorCode::BadRequest && str_contains($e->getMessage(), 'already active')) {
                Log::info('WS server error ignored: '.$e->getMessage(), [
                    'appInstance' => $this->handler->user,
                    'payload' => $payload,
                    'errorCode' => $e->errorCode,
                ]);

                return;
            }

            Log::error('WS server error: ' . $e->getMessage(), [
                'appInstance' => $this->handler->user,
                'payload' => $payload,
                'errorCode' => $e->errorCode,
            ]);

            throw new ErrorException(ErrorCode::InternalServerError, $e->getMessage());
        }

        if ($subscribed) {
            return;
        }

        throw new ErrorException(ErrorCode::InternalServerError, 'Could not subscribe to webhooks');
    }

    protected function getCallbackUrl(): string
    {
        $callbackId = $this->hub->getCallbackId($this->handler->user);
        $callbackUrl = Utils::getRetailCrmParam('wh_callback_url_base_path');
        if (!$callbackUrl) {
            return URL::route('retailcrm.callback.webhooks', ['callbackId' => $callbackId]);
        }
        if (!str_ends_with($callbackUrl, '/')) {
            $callbackUrl .= '/';
        }
        return $callbackUrl . $callbackId;
    }
}
