<?php

namespace BmPlatform\RetailCRM\Utils;

class AccountType
{
    const BOT = 'bot';
    const MANAGER= 'user';
}
