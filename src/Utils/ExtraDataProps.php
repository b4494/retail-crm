<?php

namespace BmPlatform\RetailCRM\Utils;

class ExtraDataProps
{
    const MAIN_API_KEY = 'mat';
    const CLIENT_ID = 'cid';
    const MG_BOT_API_TOKEN = 'bat';
    const MAIN_API_URL = 'mau';
    const MG_BOT_API_URL = 'bau';
    const ACCOUNT_TYPE = 'at';
    const TRANSPORT_ATTACHMENTS = 'ta';
    const EXTERNAL_MESSAGE_STATUS = 'ems';
    const MAIN_API_ID = 'mai';
    const LAST_DIALOG_ID = 'ldi';
    const IS_GROUP_MANAGER = 'igm';
    const IS_GROUP_DELIVERY_MEN = 'idm';
    const USER_GROUP_CODE = 'cd';
    const MG_CUSTOMER_EXTERNAL_ID = 'mei';
    const FIRST_NAME = 'fn';
    const LAST_NAME = 'ln';
    const USERNAME = 'un';
}
