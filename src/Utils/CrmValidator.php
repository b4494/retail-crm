<?php

namespace BmPlatform\RetailCRM\Utils;

use BmPlatform\RetailCRM\MainApiClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CrmValidator
{
    public const DOMAINS_URL = "https://infra-data.retailcrm.tech/crm-domains.json";

    public static function validateIntegrateRequest(array $data, bool $manual, bool $catalogOnly): array
    {
        if (!self::passed($errors = self::validateUrl($data['systemUrl']))) {
            Log::warning('Integration attempt with malformed url', [
                'appType' => 'retailcrm',
                'requestPayload' => $data,
            ]);

            return $errors;
        }

        if (!self::passed($errors = self::validateScopes($data['systemUrl'], $data['apiKey']))) {
            Log::warning('Integration attempt with not enough token permissions', [
                'appType' => 'retailcrm',
                'requestPayload' => $data,
            ]);

            return $errors;
        }

        if ($manual) return [];

        if (!isset($data['token'])) return [
            'error' => 'Missing validation token',
            'code' => 402,
        ];

        $errors = self::validateToken($data['apiKey'], $data['token'], $catalogOnly);

        self::passed($errors) ?: Log::warning('Integration attempt with malformed token', [
            'appType' => 'retailcrm',
            'requestPayload' => $data,
            'catalogOnly' => $catalogOnly,
        ]);

        return $errors;
    }

    public static function validateScopes(string $url, string $token): array
    {
        $httpClient = new class($url, $token) extends MainApiClient {
            protected function getDomain(): string
            {
                Str::endsWith($this->domain, '/') ?: $this->domain .= '/';
                return $this->domain . 'api/';
            }
        };
        $scopes = $httpClient->get('credentials')['scopes'];
        $missingScopes = array_diff(Utils::getRetailCrmParam('scopes'), $scopes);
        if (empty($missingScopes)) {
            return [];
        }
        return ['error' => 'The following scopes are missing for API key: ' . implode(', ', $missingScopes), 'code' => 401];
    }

    public static function validateToken(string $apiKey, string $receivedToken, bool $catalogOnly): array
    {
        $secret = Config::get('retailcrm.applications.'.($catalogOnly ? 'catalog' : 'main').'.secret');

        if (!$secret) {
            Log::error(
                'Partner profile secret is not provided. Refer to README for RETAILCRM_PARTNER_PROFILE_SECRET setting instructions',
                [ 'appType' => 'retailcrm' ]
            );

            return [ 'error' => 'Internal module configuration is malformed.', 'code' => 500 ];
        }

        if (hash_hmac('sha256', $apiKey, $secret) != $receivedToken) {
            return ['error' => 'Could not verify token integrity.', 'code' => 401];
        }

        return [];
    }

    public static function validateUrl(string $url): array
    {
        $hint = 'Please, make sure your URL corresponds to the following format: https://subdomain.host.top-level-domain/';
        $filteredUrl = filter_var($url, FILTER_VALIDATE_URL);
        if (!$filteredUrl) {
            return ['error' => 'Invalid url. ' . $hint, 'code' => 422];
        }

        $urlArray = parse_url($filteredUrl);
        if (!self::checkUrlFormat($urlArray)) {
            return ['error' => 'Invalid url format. ' . $hint, 'code' => 422];
        }

        $mainDomain = self::getMainDomain($urlArray['host']);
        $existsInCrm = self::checkDomains(self::DOMAINS_URL, $mainDomain);
        if (!$existsInCrm) {
            return ['error' => 'The provided url does not belong to a valid RetailCRM domain. ' . $hint, 'code' => 422];
        }

        return [];
    }

    public static function passed(array $validationResult): bool
    {
        return !isset($validationResult['error']);
    }

    private static function checkUrlFormat(array $crmUrl): bool
    {
        $result = true;
        $checkAuth = self::checkAuth($crmUrl);
        $checkFragment = self::checkFragment($crmUrl);
        $checkHost = self::checkHost($crmUrl);
        $checkPath = self::checkPath($crmUrl);
        $checkPort = self::checkPort($crmUrl);
        $checkQuery = self::checkQuery($crmUrl);
        $checkScheme = self::checkScheme($crmUrl);

        if (
            !$checkAuth
            || !$checkFragment
            || !$checkHost
            || !$checkPath
            || !$checkPort
            || !$checkQuery
            || !$checkScheme
        ) {
            $result = false;
        }

        return $result;
    }

    private static function checkHost(array $crmUrl): bool
    {
        return isset($crmUrl['host']);
    }

    private static function checkQuery(array $crmUrl): bool
    {
        return !(isset($crmUrl['query']) && !empty($crmUrl['query']));
    }

    private static function checkAuth(array $crmUrl): bool
    {
        return !((isset($crmUrl['pass']) && !empty($crmUrl['pass'])) || (isset($crmUrl['user']) && !empty($crmUrl['user'])));
    }

    private static function checkFragment(array $crmUrl): bool
    {
        return !(isset($crmUrl['fragment']) && !empty($crmUrl['fragment']));
    }

    private static function checkScheme(array $crmUrl): bool
    {
        return !(isset($crmUrl['scheme']) && $crmUrl['scheme'] !== 'https');
    }

    private static function checkPath(array $crmUrl): bool
    {
        return !(isset($crmUrl['path']) && $crmUrl['path'] !== '/' && $crmUrl['path'] !== '');
    }

    private static function checkPort(array $crmUrl): bool
    {
        return !(isset($crmUrl['port']) && !empty($crmUrl['port']));
    }

    private static function getValidDomains(string $domainUrl): array
    {
        try {
            $content = json_decode(file_get_contents($domainUrl), true, 512, JSON_THROW_ON_ERROR);

            return array_column($content['domains'], 'domain');
        } catch (\JsonException $e) {
            return [];
        }
    }

    private static function getMainDomain(string $host): string
    {
        $hostArray = explode('.', $host);
        unset($hostArray[0]);

        return implode('.', $hostArray);
    }

    private static function checkDomains(string $crmDomainsUrl, string $domainHost): bool
    {
        return in_array($domainHost, self::getValidDomains($crmDomainsUrl), true);
    }
}
