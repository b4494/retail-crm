<?php

namespace BmPlatform\RetailCRM\Utils;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\RetailCRM\AppHandler;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @method chatExternal(): array
 * @method messageExternal(): array
 * @method customerExternal(): array
 * @method userExternal(): array
 * @method channelExternal(): array
 * @method user(): BmPlatform\Abstraction\DataTypes\Operator
 * @method chat(): BmPlatform\Abstraction\DataTypes\Chat
 * @method message(): BmPlatform\Abstraction\DataTypes\MessageData
 * @method customer(): BmPlatform\Abstraction\DataTypes\Contact
 * @method channel(): BmPlatform\Abstraction\DataTypes\MessengerInstance
 * @method chatInstance(): string
 * @method messageInstance(): string
 * @method customerInstance(): string
 * @method userInstance(): string
 * @method channelInstance(): string
 * @method responsible(): \BmPlatform\Abstraction\DataTypes\Operator
 * @method responsibleExternal(): array|null
 * @method responsibleInstance(): string
 */
class DataWrap
{
    const DATA_KEY = 'data';
    const DATA_SOURCE_KEY = 'type';
    const DATA_SOURCE_API = 'set_from_api';

    #[ArrayShape([
        'attachment' => 'null|AttachmentEntity',
        'channel' => 'null|ChannelEntity',
        'chat' => 'null|ChatEntity',
        'customer' => 'null|CustomerEntity',
        'message' => 'null|MessageEntity',
        'user' => 'null|UserEntity',
    ])]
    protected array $entities;

    #[ArrayShape([
        'id' => 'string',
        'channel' => ['id' => 'int', 'type' => 'string', 'name' => 'string'],
        'last_dialog' => ['id' => 'int', 'responsible' => 'null|array', 'created_at' => 'string'],
        'manager' => [
            'id' => 'int',
            'external_id' => 'null|string',
            'email' => 'null|string',
            'username' => 'string',
            'first_name' => 'string'
        ],
        'customer' => [
            'id' => 'int',
            'external_id' => 'null|string',
            'type' => 'string',
            'username' => 'string',
            'name' => 'string',
            'avatar_url' => 'null|string',
            'avatar' => 'null|string',
        ],
        'created_at' => 'string',
    ])]
    protected array $externalChat;

    #[ArrayShape([
        'id' => 'int',
        'type' => 'string',
        'name' => 'string'
    ])]
    protected array $externalChannel;

    #[ArrayShape([
        'id' => 'int',
        'content' => 'null|string',
        'note' => 'null|string',
        'time' => 'string',
        'type' => 'string',
        'scope' => 'string',
        'items' => [
            'caption' => 'string',
            'type' => 'string',
            'size' => 'int',
            'preview_url' => 'null|string',
            'id' => 'int'
        ],
        'transport_attachments' => 'null|array',
        'created_at' => 'string',
    ])]
    protected array $externalMessage;

    #[ArrayShape([
        'id' => 'int',
        'external_id' => 'null|string',
        'type' => 'string',
        'username' => 'string',
        'name' => 'string',
        'avatar_url' => 'null|string',
        'avatar' => 'null|string',
    ])]
    protected array $externalCustomer;

    #[ArrayShape([
        'id' => 'int',
        'external_id' => 'null|string',
        'type' => 'null|string',
        'email' => 'null|string',
        'avatar' => 'null|string',
        'avatar_url' => 'null|string',
        'username' => 'null|string',
        'name' => 'null|string',
        'first_name' => 'null|string',
        'last_name' => 'null|string',
    ])]
    protected array $externalUser;

    protected array $externalResponsible;

    public function __construct(public readonly AppHandler $module, public readonly array $data)
    {
        //
    }

    public function timestamp(): ?Carbon
    {
        if (in_array($this->getDataSource(), ['message_new'])) {
            return Carbon::parse($this->data['data']['message']['created_at'] ?? null);
        }
        if ($this->getDataSource() === self::DATA_SOURCE_API) {
            return null;
        }
        return Carbon::createFromTimestamp($this->data['meta']['timestamp']);
    }

    public function __call(string $name, array $arguments)
    {
        $entityName = explode('_', Str::snake($name))[0];
        $class = $this->getClass($entityName);
        $propertyName = $this->getPropertyName($entityName);
        $this->setProperty($propertyName);
        if (str_contains($name, 'External')) {
            return $this->$propertyName;
        }
        if (str_contains($name, 'Instance')) {
            return $class::getExternalId($this->$propertyName);
        }
        return $this->getEntity($class, $propertyName, $entityName);
    }

    private function setExternalCustomer(): void
    {
        $this->externalCustomer = $this->chatExternal()['customer'];
    }

    private function setExternalMessage(): void
    {
        $this->externalMessage = match ($this->getDataSource()) {
            'message_new', 'message_updated' => $this->data['data']['message'],
            default => throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                __METHOD__ . ' ' . $this->getDataSource()
            ),
        };
    }

    private function setExternalChat(): void
    {
        $this->externalChat = match ($this->getDataSource()) {
            'dialog_assign', 'chat_created', 'chat_updated' => $this->data['data']['chat'],
            'dialog_closed' => $this->data['data']['dialog']['chat'],
            'message_new', 'message_updated' => $this->data['data']['message']['chat'],
            default => throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                __METHOD__ . ' ' . $this->getDataSource()
            ),
        };
    }

    private function setExternalUser(): void
    {
        $this->externalUser = match ($this->getDataSource()) {
            'message_new', 'message_updated' => $this->data['data']['message']['from'],

            default => throw new ErrorException(ErrorCode::UnexpectedData, __METHOD__ . ' ' . $this->getDataSource()),
        };
    }

    private function setExternalResponsible(): void
    {
        $this->externalResponsible = match ($this->getDataSource()) {
            'dialog_assign', 'dialog_closed' => $this->data['data']['dialog']['responsible'] ?: [],
            'message_new', 'message_updated' => $this->data['data']['message']['chat']['last_dialog']['responsible'] ?? [],
            'chat_created', 'chat_updated' => $this->data['data']['chat']['last_dialog']['responsible'] ?? [],

            default => throw new ErrorException(ErrorCode::UnexpectedData, __METHOD__ . ' ' . $this->getDataSource()),
        };
    }

    private function setExternalChannel(): void
    {
        $this->externalChannel = $this->chatExternal()['channel'];
    }

    private function getClass(string $entityName): string
    {
        return 'BmPlatform\RetailCRM\Utils\Entities\\' . ucfirst($entityName) . 'Entity';
    }

    private function getEntity(string $class, string $propertyName, string $entityName): Arrayable
    {
        return $this->entities[$entityName] ?? $this->entities[$entityName] = (new $class(
            $this->module,
            $this->$propertyName
        ))->getMappedEntity($this);
    }

    private function getPropertyName(string $entityName): string
    {
        return 'external' . ucfirst($entityName);
    }

    protected function setProperty($propertyName): void
    {
        if (!property_exists(self::class, $propertyName)) {
            throw new ErrorException(
                ErrorCode::UnexpectedServerError,
                'DataWrap doesn\'t have ' . $propertyName . ' property'
            );
        }
        if (isset($this->$propertyName)) {
            return;
        }
        if ($this->getDataSource() == self::DATA_SOURCE_API) {
            $this->$propertyName = $this->data[self::DATA_KEY];
            return;
        }
        $setter = 'set' . ucfirst($propertyName);
        $this->$setter();
    }

    private function getDataSource()
    {
        return $this->data[self::DATA_SOURCE_KEY];
    }
}
