<?php

namespace BmPlatform\RetailCRM\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Contracts\BaseEntity;
use Illuminate\Support\Facades\Config;

class ChannelEntity extends BaseEntity
{
    const REQUIRED_PRESET_FIELDS = [
        'type',
        'name',
    ];

    protected function setEntity(DataWrap $data)
    {
        $messengerType = MessengerType::tryFrom(
            Config::get('retailcrm.messenger_types.' . $this->externalEntity['type'] . '.internal_type')
        );

        $this->entity = new MessengerInstance(
            externalType: $this->externalEntity['type'],
            externalId: self::getExternalId($this->externalEntity),
            name: $name = $this->externalEntity['name'],
            messengerId: $this->mapAccount($name),
            internalType: $messengerType
        );
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }

    protected function mapAccount(mixed $name): mixed
    {
        return match ($this->externalEntity['type']) {
            'whatsapp' => preg_replace('/[^0-9]/', '', $name),
            'telegram' => mb_substr($name, 1),
            default => $name,
        };
    }
}
