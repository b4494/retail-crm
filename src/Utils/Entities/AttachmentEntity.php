<?php

namespace BmPlatform\RetailCRM\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Contracts\EntityWithAdditionalApiCalls;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class AttachmentEntity extends EntityWithAdditionalApiCalls
{
    const REQUIRED_PRESET_FIELDS = [
        'id'
    ];

    const FIELDS_WITH_DEFAULTS = [
        'type' => null,
    ];

    public function getEntityFromApi(): array
    {
        return $this->module->getBotApiClient()->get(
            'files/:file-id',
            ['params' => ['file-id' => $this->externalEntity['id']]]
        );
    }

    protected function setEntity(DataWrap $data)
    {
        $this->entity = new MediaFile(
            type: MediaFileType::fromMime($this->externalEntity['type'] ?? ''),
            // apidoc says it's url but in reality it's Url
            url: $this->externalEntity[self::API_DATA_KEY]['url'] ?? $this->externalEntity[self::API_DATA_KEY]['Url'],
            mime: $this->externalEntity['type']
        );
    }

    public static function getExternalId(array $externalEntity): ?string
    {
        throw new ErrorException(ErrorCode::FeatureNotSupported, 'External id is not supported for attachments.');
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }
}
