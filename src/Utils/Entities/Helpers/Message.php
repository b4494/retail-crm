<?php

namespace BmPlatform\RetailCRM\Utils\Entities\Helpers;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MessageStatus;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;

class Message
{
    public static function mapStatusToInternal($externalStatus): MessageStatus
    {
        return match ($externalStatus) {
            'sending' => MessageStatus::Pending,
            'sent' => MessageStatus::Sent,
            'received' => MessageStatus::Delivered,
            'failed' => MessageStatus::FailedToSend,
            'seen' => MessageStatus::Red,
            default => throw new ErrorException(
                ErrorCode::UnexpectedData,
                'Unknown message status ['.$externalStatus.']'
            ),
        };
    }

    public static function needsProcessing(array $externalMessage): bool
    {
        return $externalMessage && $externalMessage['scope'] !== 'private' && !self::sentByBot($externalMessage);
    }

    private static function sentByBot(array $externalMessage): bool
    {
        if (!isset($externalMessage['from'])) return true;

        $namesMatch = in_array($externalMessage['from']['name'], Arr::pluck(Config::get('retailcrm.applications'), 'botName'));

        return $namesMatch && $externalMessage['from']['type'] == 'bot';
    }
}
