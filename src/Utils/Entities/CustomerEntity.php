<?php

namespace BmPlatform\RetailCRM\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Contracts\BaseEntity;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\Support\Helpers;

class CustomerEntity extends BaseEntity
{
    const REQUIRED_PRESET_FIELDS = [
        'id',
    ];

    const FIELDS_WITH_DEFAULTS = [
        'avatar_url' => null,
        'avatar' => null,
        'name' => null,
        'username' => null,
    ];

    protected function setEntity(DataWrap $data)
    {
        $this->entity = new Contact(
            externalId: self::getExternalId($this->externalEntity),
            name: $this->externalEntity['name'],
            phone: $this->externalEntity['phone'] ?? null,
            avatarUrl: $this->externalEntity['avatar_url'] ?? $this->externalEntity['avatar'],
            messengerId: $this->externalEntity['external_id'] ?? null,
            extraData: $this->getExtraData(),
        );
    }

    protected function getExtraData(): array
    {
        return [
            ExtraDataProps::USERNAME => $this->externalEntity['username'],
            ExtraDataProps::FIRST_NAME => $this->externalEntity['first_name'] ?? null,
            ExtraDataProps::LAST_NAME => $this->externalEntity['last_name'] ?? null,
        ];
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }
}

