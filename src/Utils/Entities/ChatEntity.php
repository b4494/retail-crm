<?php

namespace BmPlatform\RetailCRM\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Contracts\BaseEntity;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\Support\Helpers;

class ChatEntity extends BaseEntity
{
    const REQUIRED_PRESET_FIELDS = [
        'channel',
        'customer',
    ];

    const REQUIRED_PRESET_NESTED_FIELDS = [
        'channel.id',
        'customer.id',
    ];

    const FIELDS_WITH_DEFAULTS = [
        'last_dialog.id' => null,
        'channel.name' => null,
    ];

    protected function setEntity(DataWrap $data)
    {
        $this->entity = new Chat(
            externalId: self::getExternalId($this->externalEntity),
            messengerInstance: $data->channel(),
            contact: $data->customer(),
            operator: $data->responsibleExternal() ? (isset($data->responsibleExternal()['name']) ? $data->responsible() : $data->responsibleInstance()) : null,
            messengerId: $data->customerExternal()['external_id'] ?? null,
            extraData: $this->getExtraData(),
        );
    }

    protected function getExtraData(): array
    {
        return Helpers::withoutEmptyStrings(
            [ExtraDataProps::LAST_DIALOG_ID => $this->externalEntity['last_dialog']['id']]
        );
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }
}
