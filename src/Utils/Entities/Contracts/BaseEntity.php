<?php

namespace BmPlatform\RetailCRM\Utils\Entities\Contracts;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\RetailCRM\AppHandler;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Helpers\SetsDefaults;
use BmPlatform\RetailCRM\Utils\Entities\Helpers\ValidatesRequiredPresetFields;
use Illuminate\Contracts\Support\Arrayable;

abstract class BaseEntity implements MapsToInternalEntity
{
    use ValidatesRequiredPresetFields, SetsDefaults;

    const PRIMARY_KEY = 'id';
    // value-only
    const REQUIRED_PRESET_FIELDS = [];
    // key => value pairs, nested keys using dot notation are allowed
    const FIELDS_WITH_DEFAULTS = [];
    // value-only, using dot notation to represent nested keys
    const REQUIRED_PRESET_NESTED_FIELDS = [];

    protected Arrayable $entity;

    public function __construct(protected AppHandler $module, protected array $externalEntity)
    {
    }

    public function getMappedEntity(DataWrap $data): Arrayable
    {
        if (isset($this->entity)) {
            return $this->entity;
        }
        $this->validate();
        $this->setDefaults();
        $this->setEntity($data);
        return $this->entity;
    }

    public static function getExternalId(array $externalEntity): ?string
    {
        $mgBotApiId = data_get($externalEntity, self::PRIMARY_KEY);
        if (!$mgBotApiId) {
            throw new ErrorException(
                ErrorCode::DataMissing,
                'Can not get external id: id is missing in externalEntity ' . static::class
            );
        }
        if ($parentObjectId = static::getParentObjectId($externalEntity)) {
            $parentObjectId = ':' . $parentObjectId;
        }
        return $mgBotApiId . $parentObjectId;
    }

    abstract protected function setEntity(DataWrap $data);

    // Let's say we are working with telegram
    // Channel id would be parent object id for chat; Telegram bot id would be parent object id for channel.
    abstract protected static function getParentObjectId(array $externalEntity): ?string;
}
