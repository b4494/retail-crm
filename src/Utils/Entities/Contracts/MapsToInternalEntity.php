<?php

namespace BmPlatform\RetailCRM\Utils\Entities\Contracts;

use BmPlatform\RetailCRM\Utils\DataWrap;
use Illuminate\Contracts\Support\Arrayable;

interface MapsToInternalEntity
{
    public static function getExternalId(array $externalEntity): ?string;
    public function getMappedEntity(DataWrap $data): Arrayable;
}
