<?php

namespace BmPlatform\RetailCRM\Utils\Entities\Contracts;

interface GetsEntityFromApi
{
    public function getEntityFromApi(): array;
}
