<?php

namespace BmPlatform\RetailCRM\Utils\Entities\Contracts;

use BmPlatform\RetailCRM\Utils\DataWrap;
use Illuminate\Contracts\Support\Arrayable;

abstract class EntityWithAdditionalApiCalls extends BaseEntity implements GetsEntityFromApi
{
    const API_DATA_KEY = 'api_data';

    public function getMappedEntity(DataWrap $data): Arrayable
    {
        if (isset($this->entity)) {
            return $this->entity;
        }
        $this->externalEntity[self::API_DATA_KEY] = $this->getEntityFromApi();
        $this->validate();
        $this->setDefaults();
        $this->setEntity($data);
        return $this->entity;
    }

    public function setExternalEntity(array $externalEntity)
    {
        $this->externalEntity = $externalEntity;
    }
}
