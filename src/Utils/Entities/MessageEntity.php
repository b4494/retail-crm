<?php

namespace BmPlatform\RetailCRM\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\RetailCRM\ServiceProvider;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Contracts\BaseEntity;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\Support\Helpers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;

class MessageEntity extends BaseEntity
{
    const REQUIRED_PRESET_FIELDS = [
        'id',
        'note|content|items|order|product',
    ];

    const REQUIRED_PRESET_NESTED_FIELDS = [
        'chat.id'
    ];

    const FIELDS_WITH_DEFAULTS = [
        'items' => [],
        'transport_attachments' => null,
        'content' => null,
        'note' => null,
        'status' => null,
        'product' => null,
        'time' => null,
    ];

    protected function setEntity(DataWrap $data)
    {
        $attachments = $this->externalEntity['items'] ? $this->getFiles($this->externalEntity['items'], $data) : null;

        $this->entity = new MessageData(
            externalId: self::getExternalId($this->externalEntity),
            text: $this->mapToText(),
            attachments: $attachments,
            date: Carbon::parse($this->externalEntity['created_at']),
            extraData: $this->getExtraData()
        );
    }

    protected function getExtraData(): array
    {
        return Helpers::withoutEmptyStrings([
            ExtraDataProps::TRANSPORT_ATTACHMENTS => $this->externalEntity['transport_attachments'],
            ExtraDataProps::EXTERNAL_MESSAGE_STATUS => $this->externalEntity['status'],
            'order' => $this->externalEntity['order'] ?? null,
        ]);
    }

    protected function getFiles(array $attachments, DataWrap $data): ?array
    {
        $files = [];
        $attachmentEntity = new AttachmentEntity($this->module, []);
        foreach ($attachments as $attachment) {
            $attachmentEntity->setExternalEntity($attachment);
            $files[] = $attachmentEntity->getMappedEntity($data);
        }
        return $files ?: null;
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }

    /**
     * @return mixed|null
     */
    protected function mapToText(): ?string
    {
        if (!is_null($text = $this->externalEntity['content'] ?? $this->externalEntity['note'] ?? null)) {
            return $text;
        } elseif (isset($this->externalEntity['order'])) {
            return Lang::get(
                'retailcrm::messages.outbox_order', [ 'number' => $this->externalEntity['order']['number'] ]
            );
        } elseif (isset($this->externalEntity['product'])) {
            return implode(PHP_EOL,  array_filter([
                $this->externalEntity['product']['name'],
                $this->externalEntity['product']['url'] ?? null,
            ]));
        } else {
            return null;
        }
    }
}
