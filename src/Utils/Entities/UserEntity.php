<?php

namespace BmPlatform\RetailCRM\Utils\Entities;

use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Contracts\BaseEntity;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\Support\Helpers;

class UserEntity extends BaseEntity
{
    const REQUIRED_PRESET_FIELDS = [
        'id',
        'first_name|name',
    ];

    const FIELDS_WITH_DEFAULTS = [
        'external_id' => null,
        'type' => null,
        'avatar' => null,
        'last_name' => null,
        'email' => null
    ];

    protected function setEntity(DataWrap $data)
    {
        $this->entity = new Operator(
            externalId: self::getExternalId($this->externalEntity),
            name: $this->name(),
            email: $this->externalEntity['email'] ?: null,
            avatarUrl: $this->externalEntity['avatar'] ?: null,
            extraData: $this->getExtraData()
        );
    }

    protected function getExtraData(): array
    {
        return [
            ExtraDataProps::ACCOUNT_TYPE => $this->externalEntity['type'],
            ExtraDataProps::MAIN_API_ID => $this->externalEntity['external_id'],
            ExtraDataProps::FIRST_NAME => $this->externalEntity['first_name'] ?? null,
            ExtraDataProps::LAST_NAME => $this->externalEntity['last_name'] ?? null,
        ];
    }

    protected static function getParentObjectId(array $externalEntity): ?string
    {
        return null;
    }

    private function name(): string
    {
        if (isset($this->externalEntity['name'])) return $this->externalEntity['name'];

        return implode(' ', array_filter([ $this->externalEntity['first_name'] ?? null, $this->externalEntity['last_name'] ?? null ]));
    }
}
