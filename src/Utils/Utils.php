<?php

namespace BmPlatform\RetailCRM\Utils;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Abstraction\Interfaces\Contact;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

final class Utils
{
    public static function getMgBotApiId(string $externalId): int
    {
        return (int)explode(':', $externalId)[0];
    }

    public static function getParentId(string $externalId): int
    {
        return (int)explode(':', $externalId)[1];
    }

    public static function mapAppStatus(bool $active, bool $frozen, string $appType): AppExternalStatus
    {
        if ($frozen) {
            return new AppExternalStatus(AppStatus::Frozen);
        } elseif (!$active) {
            return new AppExternalStatus(AppStatus::Disabled);
        } else {
            return new AppExternalStatus(AppStatus::Active, $appType == 'main' ? PaymentStatus::Paid : null);
        }
    }

    public static function getRetailCrmParam(string $key): mixed
    {
        $prefix = 'retailcrm_params.';
        $required = Config::get($prefix . 'required');
        $value = Config::get($prefix . $key);
        if (is_null($value)) {
            if (in_array($key, $required)) {
                Log::error(
                    'ATTENTION: ' . $prefix . $key . ' is set to null, but it is required for this app to work. Refer to README for environment variables instructions.',
                    ['appType' => 'retailcrm']
                );
            } else {
                Log::warning(
                    'ATTENTION: ' . $prefix . $key . ' is set to null. Refer to README for environment variables instructions.',
                    ['appType' => 'retailcrm']
                );
            }
        }
        return $value;
    }

    public static function resolveContactExtra(Contact $c, string $extraShort): mixed
    {
        return $c->getExtraData()[$extraShort] ?? $c->getExtraFields()[$extraShort] ?? null;
    }

    public static function extractSubdomain($url): ?string
    {
        $host = parse_url($url)['host'];
        $host = explode('.', $host);
        if (count($host) >= 3) {
            array_splice($host, -2);
        }
        return implode('.', $host);
    }
}
