<?php

namespace BmPlatform\RetailCRM\Commands;

use BmPlatform\Abstraction\Enums\QuickReplyType;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\RetailCRM\Utils\Entities\MessageEntity;
use BmPlatform\RetailCRM\Utils\Utils;

trait MessageCommands
{
    public function sendSystemMessage(SendSystemMessageRequest $request): void
    {
        // TODO: figure out whether system message means that the message will only be seen by operators on the platform
        $additionalPayload = [ 'content' => $request->text ];
        $this->sendMessage($request->chat, 'private', 'text', $additionalPayload);
    }

    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult
    {
        $additionalPayload = [
            'content' => $request->text,
        ];
        if ($suggestions = $this->getQuickReplies($request->quickReplies)) {
            $additionalPayload = $additionalPayload + [
                    'transport_attachments' => [ 'suggestions' => $suggestions ],
                ];
        }
        $result = $this->sendMessage($request->chat, 'public', 'text', $additionalPayload);
        return new MessageSendResult(
            MessageEntity::getExternalId(
                [
                    'id' => $result['message_id'],
                    'chat' => [ 'id' => Utils::getMgBotApiId($request->chat->getExternalId()) ],
                ]
            )
        );
    }

    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult
    {
        $fileId = $this->module->getBotApiClient()->post('files/upload_by_url', [
            'json' => [ 'url' => $request->file->url ],
        ])['id'];

        $payload = [
            'note' => $request->caption,
            'items' => [ [ 'id' => $fileId, 'caption' => $request->file->name ?: pathinfo($request->file->url, PATHINFO_FILENAME) ] ],
        ];

        if ($suggestions = $this->getQuickReplies($request->quickReplies)) {
            $payload['transport_attachments'] = compact('suggestions');
        }

        $result = $this->sendMessage($request->chat, 'public', $this->fileTypeToMessageType($request->file), $payload);

        return new MessageSendResult(
            MessageEntity::getExternalId([
                'id' => $result['message_id'],
                'chat' => [ 'id' => Utils::getMgBotApiId($request->chat->getExternalId()) ],
            ])
        );
    }

    private function getQuickReplies(array $quickReplies): array
    {
        $suggestions = [];

        foreach ($quickReplies as $replies) {
            foreach ($replies as $reply) {
                if ('text' === $type = $this->matchQuickReplyType($reply->type)) {
                    $suggestions[] = [
                        'title' => $reply->text,
                        'type' => $type,
                    ];
                } elseif ($type) {
                    $suggestions[] = [ 'type' => $type ];
                }

            }
        }

        return $suggestions;
    }

    private function matchQuickReplyType(QuickReplyType $type): ?string
    {
        return match ($type) {
            QuickReplyType::Text => 'text',
            QuickReplyType::Phone => 'phone',
            default => null
        };
    }

    private function sendMessage(Chat $chat, string $scope, string $type, array $additionalPayload): array
    {
        return $this->module->getBotApiClient()->post('messages', [
            'json' => [
                    'chat_id' => Utils::getMgBotApiId($chat->getExternalId()),
                    'scope' => $scope,
                    'type' => $type,
                ] + $additionalPayload,
        ]);
    }
}
