<?php

namespace BmPlatform\RetailCRM\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\OperatorGroup;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\RetailCRM\Utils\AccountType;
use BmPlatform\RetailCRM\Utils\Entities\UserEntity;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\RetailCRM\Utils\Utils;

trait OperatorCommands
{
    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse
    {
        if (($operator = $request->target) instanceof OperatorGroup) {
            throw new ErrorException(ErrorCode::FeatureNotSupported, 'Chat transfer to operator groups is not supported by retailcrm');
        }

        $dialog = $this->getLastDialog($request->chat);

        $id = Utils::getMgBotApiId($operator->getExternalId());
        $type = $operator->getExtraData()[ExtraDataProps::ACCOUNT_TYPE];

        if (isset($dialog['responsible']) && $dialog['responsible']['type'] == $type && $dialog['responsible']['id'] == $id) {
            throw new ErrorException(ErrorCode::OperatorAlreadyAssigned);
        }

        try {
            $response = $this->module->getBotApiClient()->patch('dialogs/:id/assign', [
                'params' => [ 'id' => $dialog['id'] ],
                'json' => [ "{$type}_id" => $id ],
            ]);

            return new NewOperatorResponse(UserEntity::getExternalId([ 'id' => $response['responsible']['id'] ]));
        }

        catch (ErrorException $e) {
            if (str_contains(strtolower($e->getMessage()), 'already')) {
                throw new ErrorException(ErrorCode::OperatorAlreadyAssigned);
            }

            throw $e;
        }
    }
}
