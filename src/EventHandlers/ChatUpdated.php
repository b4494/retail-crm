<?php

namespace BmPlatform\RetailCRM\EventHandlers;

use BmPlatform\Abstraction\Events\ChatDataChanged;
use BmPlatform\RetailCRM\Utils\DataWrap;

class ChatUpdated
{
    public function __invoke(DataWrap $data): ChatDataChanged
    {
        return new ChatDataChanged(
            chat: $data->chat(),
            timestamp: $data->timestamp()
        );
    }
}
