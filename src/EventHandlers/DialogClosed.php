<?php

namespace BmPlatform\RetailCRM\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\RetailCRM\Utils\DataWrap;

class DialogClosed
{
    public function __invoke(DataWrap $data): ChatTicketClosed
    {
        return new ChatTicketClosed(
            chat: $data->chat(),
            timestamp: $data->timestamp()
        );
    }
}
