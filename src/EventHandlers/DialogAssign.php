<?php

namespace BmPlatform\RetailCRM\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTransferredToOperator;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\UserEntity;

class DialogAssign
{
    public function __invoke(DataWrap $data): ChatTransferredToOperator
    {
        return new ChatTransferredToOperator(
            chat: $data->chat(),
            // Calling https://139810.selcdn.ru/download/doc/mg-bot-api/bot.v1.ru.html#_dialog_unassign method will set responsible to null
            newOperator: $data->responsibleExternal() ? $data->responsibleInstance() : null,
        );
    }
}
