<?php

namespace BmPlatform\RetailCRM\EventHandlers;

use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Abstraction\Events\MessageStatusChanged;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Entities\Helpers\Message;
use BmPlatform\RetailCRM\Utils\Entities\MessageEntity;

class MessageUpdated
{
    public function __invoke(DataWrap $data): ?Event
    {
        if (!Message::needsProcessing($message = $data->messageExternal())) {
            return null;
        }

        return new MessageStatusChanged(
            chat: $data->chat(),
            externalId: MessageEntity::getExternalId($message),
            status: Message::mapStatusToInternal($message['status']),
            timestamp: $data->timestamp()
        );
    }
}
