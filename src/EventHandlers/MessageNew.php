<?php

namespace BmPlatform\RetailCRM\EventHandlers;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Events\ChatDataChanged;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\RetailCRM\Utils\DataWrap;
use Carbon\Carbon;

class MessageNew
{
    public function __invoke(DataWrap $data): ?Event
    {
        $msg = $data->messageExternal();

        return match ($msg['scope']) {
            'public' => $this->handlePublicMessage($msg, $data),
            'private' => $this->handlePrivateMessage($msg, $data),
            default => throw new ErrorException(ErrorCode::UnexpectedData, 'Unknown message scope ['.$msg['scope'].']'),
        };
    }

    protected function getFlags(array $chat, array $message): int
    {
        $flags = 0;

        $messageCreated = Carbon::parse($message['created_at']);
        $chatCreated = Carbon::parse($chat['created_at']);
        $newChat = $messageCreated->diffInSeconds($chatCreated) <= 1;
        $newDialog = false;
        if (isset($chat['last_dialog']['created_at'])) {
            $dialogCreated = Carbon::parse($chat['last_dialog']['created_at']);
            $newDialog = $messageCreated->diffInSeconds($dialogCreated) <= 1;
        }
        if ($newChat) {
            $flags |= InboxFlags::NEW_CHAT_CREATED;
            $flags |= InboxFlags::NEW_TICKET_OPENED;
            return $flags;
        }
        if ($newDialog) {
            $flags |= InboxFlags::NEW_TICKET_OPENED;
        }

        return $flags;
    }

    protected function handlePublicMessage(array $msg, DataWrap $data): ?Event
    {
        return match ($msg['from']['type']) {
            'customer' => new InboxReceived(
                chat: $data->chat(),
                participant: $data->customer(),
                message: $data->message(),
                flags: $this->getFlags($data->chatExternal(), $msg),
                timestamp: $data->timestamp()
            ),

            'user' => new OutboxSent(
                chat: $data->chat(),
                message: $data->message(),
                operator: $data->user(),
                timestamp: $data->timestamp()
            ),

            'bot', 'channel' => null,

            default => throw new ErrorException(
                ErrorCode::UnexpectedData, 'Unknown message sender ['.$msg['from']['type'].']'
            ),
        };
    }

    protected function handlePrivateMessage(array $msg, DataWrap $data): ?Event
    {
        return match (true) {
            // Handle case when bot opens new dialog, we need to update last dialog id
            $msg['type'] == 'system' && $msg['action'] == 'dialog_opened' && ($msg['from']['type'] ?? null) == 'bot' => new ChatDataChanged(
                chat: $data->chat(),
                timestamp: $data->timestamp(),
            ),

            default => null,
        };
    }
}
