<?php

namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\AccountType;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Abstraction\Enums\PaymentType;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\RetailCRM\Utils\Utils;
use Carbon\CarbonTimeZone;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class Integrator
{
    protected static array $systemInfo;

    public function __construct(public readonly array $data, public readonly bool $catalogOnly)
    {
    }

    public function getPrematureIntegrationInstance(): AppIntegrationData
    {
        return new AppIntegrationData(
            type: 'retailcrm',
            externalId: $this->getExternalId(),
            name: $this->getName(),
            extraData: [
                ExtraDataProps::MAIN_API_URL => $this->data['systemUrl'],
                ExtraDataProps::MAIN_API_KEY => $this->data['apiKey'],
            ],
            paymentType: $this->catalogOnly ? PaymentType::Internal : PaymentType::External,
            accountType: $this->catalogOnly ? AccountType::CatalogOnly : AccountType::Full,
        );
    }

    public function getCompleteIntegrationInstance(string $callbackId): AppIntegrationData
    {
        $client = $this->createMainApiClient();
        $appConfig = Config::get('retailcrm.applications.'.($this->catalogOnly ? 'catalog' : 'main'));

        $oldestAdmin = $this->getOldestAdminUser($client);
        $email = $oldestAdmin['email'] ?? null;
        $this->validateRetrievedData(compact('email'));

        $bot = $this->initializeBot($client, $appConfig['code'], $appConfig['botName'], $callbackId);

        $botApiUrl = $bot['info']['mgBot']['endpointUrl'] ?? null;
        $botApiToken = $bot['info']['mgBot']['token'] ?? null;
        $this->validateRetrievedData(compact('botApiUrl', 'botApiToken'));

        return new AppIntegrationData(
            type: 'retailcrm',
            externalId: $this->getExternalId(),
            name: $this->getName(),
            email: $email,
            extraData: [
                ExtraDataProps::MG_BOT_API_URL => $botApiUrl,
                ExtraDataProps::MG_BOT_API_TOKEN => $botApiToken,
            ],
            paymentType: $this->catalogOnly ? PaymentType::Internal : PaymentType::External,
            status: new AppExternalStatus(AppStatus::Active, $this->catalogOnly ? null : PaymentStatus::Paid),
            accountType: $this->catalogOnly ? AccountType::CatalogOnly : AccountType::Full,
            // we assign status using appInstance->updateStatus.
        );
    }

    protected function getOldestAdminUser(MainApiClient $client): array
    {
        $admins = $client->get('users', [
            'params' => [
                'filter[isAdmin]' => '1'
            ]
        ]);

        return Arr::last($admins['users'] ?? []);
    }

    protected function initializeBot(MainApiClient $client, string $botCode, string $botName, string $callbackId): array
    {
        return $client->post('integration-modules/:bot-code/edit', [
            'params' => [ 'bot-code' => $botCode ],
            'json' => [
                'integrationModule' =>
                    json_encode([
                        'integrationCode' => $botCode,
                        'code' => $botCode,
                        'active' => true,
                        'clientId' => $callbackId,
                        'baseUrl' => URL::route('retailcrm.callback.base'),
                        'actions' => ['activity' => 'activity'],
                        'accountUrl' => URL::route('retailcrm.callback.account'),
                        'name' => $botName,
                        'integrations' => ['mgBot' => []],
                    ])
            ]
        ]);
    }

    protected function validateRetrievedData(array $retrievedData): void
    {
        if (count($retrievedData) != count(array_filter($retrievedData))) {
            Log::warning(
                'Failed to retrieve data needed for integration(empty fields in retrieved data).',
                [
                    'retrievedData' => $retrievedData,
                    'appType' => 'retailcrm',
                    'apiKey' => $this->data['apiKey']
                ]
            );
            throw new ErrorException(
                ErrorCode::IntegrationNotPossible,
                'Could not retrieve crucial data for integration!'
            );
        }
    }

    protected function createMainApiClient(): MainApiClient
    {
        return new MainApiClient($this->data['publicUrl'], $this->data['apiKey']);
    }

    public static function getSystemInfo(string $url): ?array
    {
        if (isset(self::$systemInfo)) {
            return self::$systemInfo;
        }

        $httpClient = new class($url) extends MainApiClient {
            public function __construct(public string $domain, string $token = '', array $options = [])
            {
                parent::__construct($this->domain, $token, $options);
            }

            protected function getDomain(): string
            {
                Str::endsWith($this->domain, '/') ?: $this->domain .= '/';
                return $this->domain . 'api/';
            }
        };

        self::$systemInfo = $httpClient->get('system-info');
            self::$systemInfo['publicUrl'] ?? throw new ErrorException(
            ErrorCode::IntegrationNotPossible,
            'Could not retrieve crucial data for integration!'
        );

        return self::$systemInfo;
    }

    /**
     * @return mixed
     */
    protected function getName(): mixed
    {
        return parse_url($this->data['publicUrl'], PHP_URL_HOST);
    }

    /**
     * @return string|null
     */
    protected function getExternalId(): ?string
    {
        return Utils::extractSubdomain($this->data['systemUrl']).($this->catalogOnly ? ':catalog' : '');
    }
}















