<?php

namespace BmPlatform\RetailCRM;

class AppendCustomFields
{
    public function __invoke(AppHandler $handler, array $properties, bool $byUser): array
    {
        $data = $handler->user->getTemporaryData($byUser);

        if (!is_array($data->data)) return $properties;

        foreach ($data->data['cf'] as $key => $cf) {
            $properties['CF_'.$key] = [
                'title' => $cf[0],
                'type' => $cf[1],
                ...($cf[2] ?? []),
                'isCustom' => true,
                'variableRef' => 'contact.customer.customFields.'.$key,
                ...(in_array($cf[1], [ 'date', 'datetime', 'integer', 'numeric' ]) ? [ 'variables' => true ] : []),
            ];
        }

        return $properties;
    }
}