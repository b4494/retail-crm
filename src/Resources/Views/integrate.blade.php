<!doctype html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Аутентификация</title>

    <meta name="csrf-token" content="QKWhCENzUuO88mje6m5a5Rbzq5UgNlLFWvgdILu8">

    <link href="http://fonts.cdnfonts.com/css/gotham-pro?styles=24950" rel="stylesheet">
    <link href="https://m.bot-marketing.com/css/base/app.css" rel="stylesheet" type="text/css">
</head>

<style>
    body {
        font-family: 'Gotham Pro', sans-serif;
    }

    .form-group > label {
        font-weight: 400;
    }

    form #key_api {
        border-radius: 0;
        border: 2px solid #000;
    }

    form .btn {
        color: #fff;
        background: #1B5CFF;
        border-radius: 0;
        border: 2px solid #1B5CFF;
    }

    form .btn:hover {
        color: #000;
        background: #fff;
    }
</style>

<body>
<div id="app">
    <div class="container-fluid">
        <div class="row vh-100" id="app">
            <div class="col-lg-8 d-none d-lg-block">
                <div class="p-3 d-flex flex-column" style="min-height: 100%">
                    <div class="mb-5">
                        <span class="marketplace-logo" alt="BotMarketing"></span>
                    </div>
                    <div id="generalErrors"></div>
                    <div class="row mb-5 flex-grow-1">
                        <div class="col-xl-12 d-flex align-items-center justify-content-center">
                            <div class="text-center">
                                Интеграция с сервисом <strong><em><span>#ServiceName</span></em></strong> для
                                подключения конструктора вам нужно
                                <strong><em><span>#Инструкция</span></em></strong>
                            </div>
                        </div>
                    </div>
                    <div class="mt-auto text-muted small">
                        <div class="text-nowrap mb-3">&copy; 2022 BotMarketing 18+</div>
                        <div><span class="text-nowrap">+7 (999) 333-42-35</span>, <a
                                href="mailto:info@bot-marketing.com" class="text-nowrap">info@bot-marketing.com</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 bg-white">
                <div class="d-flex flex-column align-items-center h-100 mx-auto" style="width:280px">
                    <div class="d-none d-lg-block mb-5" style="height:35px"></div>

                    <form class="d-flex flex-column justify-content-center flex-grow-1">
                        <div class="form-group">
                            <label for="systemUrl">Введите Ваш retailCrm домен:</label>
                            <input type="text" class="form-control" id="systemUrl" name="systemUrl" value="">
                            <div id="systemUrlErrors"></div>
                        </div>
                        <div class="form-group">
                            <label for="apiKey">Введите Api ключ:</label>
                            <input type="text" class="form-control" id="apiKey" name="apiKey" value="">
                            <div id="apiKeyErrors"></div>
                        </div>
                        <button type="button" class="btn" onclick="integrate()">Войти</button>
                    </form>

                    <div class="mt-4 mb-3 text-muted">
                        <small>Нажимая кнопку «Войти», вы подтверждаете, что ознакомлены и согласны с
                            условиями<br><a href="/files/pers_bm.pdf" target="_blank">Соглашения о пользовании
                                системой</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>

<script>
    function integrate() {
        hideErrors();
        let systemUrl = document.getElementById('systemUrl').value;
        let apiKey = document.getElementById('apiKey').value;
        let body = {register: {apiKey: apiKey, systemUrl: systemUrl}};

        if (!systemUrl) {
            displayError('systemUrlErrors', 'This field is required!')
        }
        if (!apiKey) {
            displayError('apiKeyErrors', 'This field is required!');
        }
        if (!systemUrl || !apiKey) {
            return;
        }
        fetch('{{\Illuminate\Support\Facades\URL::route('retailcrm.integration')}}', {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
        })
            .then((response) => response.json(), (reason) => displayError('generalErrors', 'Internal server error on integration request.'))
            .then((data) => {
                if (!data) {
                    return;
                }
                if (!data.success) {
                    let error = data.errorMsg;
                    let lowerError = error.toLowerCase();
                    let blockId = 'generalErrors';
                    if (lowerError.includes('url') || lowerError.includes('domain')) {
                        blockId = 'systemUrlErrors';
                    }
                    if (lowerError.includes('key') || lowerError.includes('token') || lowerError.includes('api')) {
                        blockId = 'apiKeyErrors';
                    }
                    displayError(blockId, error);
                    return;
                }
                if (!data.accountUrl) {
                    displayError('generalErrors', 'Malformed successful response. Please, contact support.');
                    return;
                }
                window.location = data.accountUrl;
            })
            .catch((err) => displayError('generalErrors', 'Internal error.'));
    }

    function displayError(blockId, error) {
        document.getElementById(blockId).innerHTML = '<div class="alert alert-danger"> ' + error + ' </div>';
    }

    function hideErrors() {
        document.getElementById('apiKeyErrors').innerHTML = '';
        document.getElementById('generalErrors').innerHTML = '';
        document.getElementById('systemUrlErrors').innerHTML = '';
    }
</script>
