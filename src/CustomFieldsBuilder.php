<?php

namespace BmPlatform\RetailCRM;

class CustomFieldsBuilder
{
    public function __construct(protected readonly MainApiClient $client)
    {
        //
    }

    public function __invoke()
    {
        return $this->makeCustomFields($this->getDictionaries());
    }

    /** @return array<string, array> */
    protected function getDictionaries(): array
    {
        $result = [];

        do {
            $data = $this->client->all('custom-fields/dictionaries', isset($data) ? $data->cursor : 1);

            foreach ($data->data['customDictionaries'] as $dict) {
                if (count($dict['elements']) > 30) continue;

                $result[$dict['code']] = array_map(
                    fn($v) => [ 'label' => $v['name'], 'value' => $v['code'] ],
                    $dict['elements'],
                );
            }
        } while ($data->cursor);

        return $result;
    }

    protected function makeCustomFields(array $dicts): array
    {
        $result = [];
        $options = [ 'query' => [ 'filter' => [ 'entity' => 'customer' ] ] ];

        do {
            $data = $this->client->all('custom-fields', isset($data) ? $data->cursor : 1, $options);

            foreach ($data->data['customFields'] as $f) {
                if ($config = $this->mapCustomField($f, $dicts)) $result[$f['code']] = $config;
            }
        } while ($data->cursor);

        return $result;
    }

    protected function mapCustomField(array $f, array $dicts): ?array
    {
        $isDict = $f['type'] == 'dictionary' || $f['type'] == 'multiselect_dictionary';

        if ($isDict && !isset($dicts[$f['dictionary']])) {
            $f['type'] = 'string';
            $isDict = false;
        }

        return array_filter([
            $f['name'],

            match ($f['type']) {
                'boolean', 'text', 'date', 'datetime', 'string' => $f['type'],
                'integer' => 'int',
                'numeric' => 'number',
                'dictionary', 'multiselect_dictionary' => 'enum',
                default => 'string',
            },

            ($isDict ? [
                'options' => $dicts[$f['dictionary']],
                ...($f['type'] == 'multiselect_dictionary' ? [ 'multiple' => true ] : []),
            ] : []),
        ]);
    }
}