<?php

namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Commands\SendsSystemMessages;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTickets;
use BmPlatform\Abstraction\Interfaces\Commands\TransfersChatToOperator;
use BmPlatform\Abstraction\Interfaces\Commands\UpdatesContactData;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\RetailCRM\Commands\MessageCommands;
use BmPlatform\RetailCRM\Commands\OperatorCommands;

class Commands implements CommandsInterface, SendsSystemMessages, TransfersChatToOperator,
                          SupportsChatTickets, UpdatesContactData
{
    use MessageCommands, OperatorCommands;

    public function __construct(public readonly AppHandler $module)
    {
    }

    public function updateContactData(UpdateContactDataRequest $request): ?Contact
    {
        if (!$customer = $this->module->resolveCustomer($request->contact)) {
            throw new ErrorException(ErrorCode::CustomerNotFound, 'Customer is not attached to this chat');
        }

        $payload = [];

        foreach ($request->data as $key => $value) {
            if ($key == 'addNote') continue;

            if ($key == 'phone') {
                $payload['phones'] = [ [ 'number' => $value ] ];

                continue;
            }

            $value = match ($key) {
                'birthday' => $value->format('Y-m-d'),
                default => $value,
            };

            [ $obj, $prop ] = explode('_', $key, 2) + [ 1 => null ];

            if ($prop) {
                $payload[$obj == 'CF' ? 'customFields' : $obj][$prop] = $value;
            } else {
                $payload[$obj] = $value;
            }
        }

        $this->module->getMainApiClient()->post('customers/:id/edit', [
            'params' => [ 'id' => $customer['id'] ],
            'json' => [ 'by' => 'id', 'site' => $customer['site'], 'customer' => json_encode($payload) ],
        ]);

        if ($note = $request->data['addNote'] ?? null) {
            $this->module->getMainApiClient()->post('customers/notes/create', [
                'json' => [
                    'site' => $customer['site'],
                    'note' => json_encode([
                        'customer' => [ 'id' => $customer['id'] ],
                        'text' => $note,
                    ]),
                ],
            ]);
        }

        return null;
    }

    public function closeChatTicket(Chat $chat): void
    {
        $dialog = $this->getLastDialog($chat);

        if (!$dialog['is_active']) {
            throw new ErrorException(ErrorCode::ChatTicketAlreadyClosed);
        }

        try {
            $this->module->getBotApiClient()->delete('dialogs/:id/close', [
                'params' => [ 'id' => $dialog['id'] ]
            ]);
        } catch (ErrorException $e) {
            if (str_contains(strtolower($e->getMessage()), 'already')) {
                throw new ErrorException(ErrorCode::ChatTicketAlreadyClosed, $e->getMessage());
            }

            throw $e;
        }
    }

    public function fileTypeToMessageType(MediaFile $file): string
    {
        return match ($file->type) {
            MediaFileType::Image => 'image',
            MediaFileType::Audio => 'audio',
            default => 'file',
        };
    }

    /**
     * @param \BmPlatform\Abstraction\Interfaces\Chat $chat
     *
     * @return array{is_active: bool, id: int, responsible: array, is_assigned: bool}
     */
    protected function getLastDialog(Chat $chat): array
    {
        $result = $this->module->getBotApiClient()->get('dialogs', [
            'query' => [ 'chat_id' => $chat->getExternalId(), 'limit' => 1 ],
        ]);

        if (!$result) throw new ErrorException(ErrorCode::UnexpectedData, 'Chat dialogs are missing');

        return $result[0];
    }
}
