<?php

use Illuminate\Support\Facades\Route;
use BmPlatform\RetailCRM\Http\IntegrationController;
use BmPlatform\RetailCRM\Http\CallbackController;

Route::namespace('BmPlatform\RetailCRM\Http')->name('retailcrm.')->group(function () {
    Route::post('integrations/retailcrm/webhook/{callbackId}',  [CallbackController::class, 'webhook'])->name('callback.webhooks');
    Route::post('integrations/retailcrm/integrate',             [IntegrationController::class, 'integrate'])->name('integration');
    Route::post('integrations/retailcrm/account',               [CallbackController::class, 'account'])->name('callback.account');
    Route::get('integrations/retailcrm/integrate',              [IntegrationController::class, 'manualIntegration'])->name('integration.view');
    Route::post('integrations/retailcrm/activity',               [CallbackController::class, 'activity'])->name('callback.activity');
    Route::get('integrations/retailcrm/config',                 [CallbackController::class, 'config'])->name('callback.config');
    Route::get('integrations/retailcrm',                        fn() => response()->json(['success' => true]))->name('callback.base');
});

