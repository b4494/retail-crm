<?php

namespace BmPlatform\RetailCRM\Http;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\RetailCRM\EventHandler;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\Utils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class CallbackController
{
    public function webhook(Request $request, Hub $hub, $callbackId): void
    {
        $data = $request->input();

        // Пропустить рассылку, чтобы не нагружать CPU
        if ((($data['message'] ?? $data['data']['message'] ?? [])['from']['type'] ?? null) == 'channel') return;

        if (!$appInstance = $hub->findAppByCallbackId('retailcrm', $callbackId)) return;

        Log::info('WH', [
            'data' => $data,
            'appInstance' => $appInstance,
            ...($request->hasHeader('X-Receive-Time') ? [
                'lat' => Carbon::parse($request->header('X-Receive-Time'))?->floatDiffInSeconds(),
            ] : []),
        ]);

        foreach ((new EventHandler())->getEvents(new DataWrap($appInstance->getHandler(), $data)) as $event) {
            if ($event) $appInstance->dispatchEvent($event);
        }
    }

    public function account(Request $request, Hub $hub)
    {
        if (!$request->input('clientId')) {
            return response()->json([
                'success' => false,
                'errorMsg' => 'clientId must be present in the payload.'
            ], 422);
        }

        if (!$appInstance = $hub->findAppByCallbackId('retailcrm', $request->input('clientId'))) {
            Log::error(
                __METHOD__ . ' Failed to resolve app by callback for user account',
                ['requestPayload' => $request->all(), 'appType' => 'retailcrm']
            );

            return response()->json(['success' => false, 'errorMsg' => 'Unknown clientId.'], 404);
        }

        return redirect($hub->webUrl($hub->getAuthToken($appInstance)));
    }

    public function activity(Request $request, Hub $hub)
    {
        if (!$callbackId = $request->input('clientId')) {
            Log::error('ClientId request came in without clientId set');

            return response()->json(['success' => false, 'errorMsg' => 'clientId must be present in the payload.'], 422);
        }

        if (!$appInstance = $hub->findAppByCallbackId('retailcrm', $callbackId)) {
            Log::error(
                __METHOD__ . ' Failed to resolve app by callback for user account',
                ['requestPayload' => $request->all(), 'appType' => 'retailcrm']
            );

            return response()->json(['success' => false]);
        }

        Log::info('Activity callback', [ 'appInstance' => $appInstance, 'data' => $request->all() ]);

        $activity = json_decode($request->input('activity'), true);

        $frozen = $activity['freeze']
            ?? throw new ErrorException(ErrorCode::UnexpectedServerError, 'Unexpected payload received: flag freeze is absent');

        $active = $activity['active']
            ?? throw new ErrorException(ErrorCode::UnexpectedServerError, 'Unexpected payload received: flag active is absent');

        /** @var \BmPlatform\RetailCRM\AppHandler $appHandler */
        $appHandler = $appInstance->getHandler();

        $appInstance->updateStatus(Utils::mapAppStatus((bool)$active, (bool)$frozen, $appHandler->getAppType()));

        return response()->json([ 'success' => true ]);
    }

    public function config(Request $request)
    {
        return response()->json([
            'scopes' => Utils::getRetailCrmParam('scopes'),
            'success' => true,
            'registerUrl' => URL::route('retailcrm.integration').($request->input('catalogOnly') ? '?catalogOnly=1' : ''),
        ]);
    }
}
