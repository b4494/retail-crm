<?php

namespace BmPlatform\RetailCRM\Http;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\RetailCRM\Integrator;
use BmPlatform\RetailCRM\MainApiClient;
use BmPlatform\RetailCRM\Utils\CrmValidator;
use BmPlatform\RetailCRM\Utils\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class IntegrationController
{
    public function manualIntegration()
    {
        return view('retailcrm::integrate');
    }

    public function integrate(Request $request, Hub $hub)
    {
        if (!$request->filled('register')) {
            Log::warning('Missing register data', [
                'appType' => 'retailcrm',
                'requestPayload' => $request->all(),
            ]);

            return [ 'error' => 'Required fields are missing in the request payload.', 'code' => 422 ];
        }

        $data = $request->input('register');

        if (!($manualIntegration = !is_string($data))) {
            $data = json_decode($data, true);
        }

        $validated = CrmValidator::validateIntegrateRequest($data, $manualIntegration, $catalogOnly = !!$request->input('catalogOnly'));

        if (!CrmValidator::passed($validated)) {
            return response()->json(['success' => false, 'errorMsg' => $validated['error']], $validated['code']);
        }

        try {
            $systemInfo = Integrator::getSystemInfo($data['systemUrl']);
        } catch (ErrorException $e) {
            $response = $this->mapInternalErrorToResponse($e);

            return response()->json(['success' => false, 'errorMsg' => $response['error']], $response['code']);
        }

        if (!isset($systemInfo['publicUrl']) || !isset($systemInfo['technicalUrl'])) {
            Log::warning('Could not retrieve crm domains from getSystemInfo.', [
                'appType' => 'retailcrm',
                'requestPayload' => $request->all(),
                'getSystemInfoResult' => $systemInfo,
            ]);

            return response()->json(['success' => false, 'errorMsg' => 'Could not retrieve crucial data.'], 500);
        }

        $integrator = new Integrator([
            'publicUrl' => $systemInfo['publicUrl'],
            'systemUrl' => $systemInfo['technicalUrl'],
            'apiKey' => $data['apiKey'],
        ], $catalogOnly);

        try {
            $appInstance = $hub->integrate($integrator->getPrematureIntegrationInstance());

            $callbackId = $hub->getCallbackId($appInstance);

            $hub->integrate($integrator->getCompleteIntegrationInstance($callbackId));
        } catch (ErrorException $e) {
            if ($e->errorCode == ErrorCode::InsufficientFunds) {
                // app instance must be defined because we can only get insufficient funds from getCompleteIntegrationInstance
                $appInstance?->updateStatus(new AppExternalStatus(AppStatus::Frozen));
            }

            $response = $this->mapInternalErrorToResponse($e);

            return response()->json([ 'success' => false, 'errorMsg' => $response['error'] ], $response['code']);
        }

        catch (\Throwable $e) {
            $response = $this->mapInternalErrorToResponse($e);

            return response()->json([ 'success' => false, 'errorMsg' => $response['error'] ], $response['code']);
        }

        if ($manualIntegration) {
            return response()->json([
                'success' => true,
                'accountUrl' => $hub->webUrl($hub->getAuthToken($appInstance)),
            ]);
        }

        return response()->json([
            'success' => true,
            'accountUrl' => URL::route('retailcrm.callback.account'),
        ]);
    }

    protected function mapInternalErrorToResponse(\Throwable $e): array
    {
        $response = false;
        Log::error(get_class($e) . ': User integration was rejected! ' . $e->getMessage(), ['appType' => 'retailcrm', 'errorMessage' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
        if ($e instanceof ErrorException) {
            $response = match ($e->errorCode ?? null) {
                ErrorCode::IntegrationNotPossible   => ['error' => 'Could not retrieve the required data from your RetailCRM account.', 'code' => 500],
                ErrorCode::InsufficientFunds        => ['error' => 'Not enough money on your account to make a payment.', 'code' => 402],
                ErrorCode::AuthenticationFailed     => ['error' => 'Could not retrieve data with provided API token.', 'code' => 403],
                ErrorCode::UnexpectedServerError    => ['error' => 'Error while setting up your module instance.', 'code' => 500],
                ErrorCode::DataMissing,             => ['error' => 'Internal module configuration is malformed.', 'code' => 500],
                default                             => false,
            };
        }
        if (!$response) {
            $response = ['error' => 'Unexpected error.', 'code' => 500];
        }
        return $response;
    }
}
