<?php

namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\RetailCRM\Utils\DataWrap;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;

class EventHandler
{
    private function getClass($webhookType): string
    {
        return 'BmPlatform\RetailCRM\EventHandlers\\' . Str::studly($webhookType);
    }

    /** @return \BmPlatform\Abstraction\Events\Event[] */
    public function getEvents(DataWrap $data): array
    {
        if (!$handler = $this->resolveHandler($data)) return [];

        if (!$result = $handler($data)) return [];

        return $result instanceof \Traversable ? iterator_to_array($result) : [ $result ];
    }

    public function resolveHandler(DataWrap $data): mixed
    {
        $type = $data->data['type'] ?? '';

        if (!in_array($type, SetWebhook::EVENTS) || !class_exists($class = $this->getClass($type))) return null;

        return new $class;
    }
}
