<?php

namespace BmPlatform\RetailCRM;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\OperatorGroup;
use BmPlatform\Abstraction\DataTypes\TemporaryData;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\Features\HasOperatorGroups;
use BmPlatform\Abstraction\Interfaces\Features\HasOperators;
use BmPlatform\Abstraction\Interfaces\AppHandler as AppHandlerInterface;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Features\SupportsTemporaryData;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\RetailCRM\Utils\AccountType;
use BmPlatform\RetailCRM\Utils\DataWrap;
use BmPlatform\RetailCRM\Utils\ExtraDataProps;
use BmPlatform\RetailCRM\Utils\Utils;
use Carbon\Carbon;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class AppHandler implements AppHandlerInterface, HasOperators, HasOperatorGroups, SupportsTemporaryData
{
    public function __construct(
        public readonly AppInstance $user,
        protected Repository $config,
        private ?MainApiClient $mainApiClient = null,
        private ?BotApiClient $botApiClient = null,
        private ?Commands $commands = null,
    ) {
        //
    }

    public function getMessengerInstances(mixed $cursor = null): IterableData
    {
        if (!$this->isCompleteInstance()) return new IterableData([], null);

        return new IterableData(
            collect(
                $this->getBotApiClient()->get('channels', [
                    'query' => [
                        'active' => 1
                    ]
                ])
            )->map(
                function (array $channel) {
                    $channel = [DataWrap::DATA_KEY => $channel, DataWrap::DATA_SOURCE_KEY => DataWrap::DATA_SOURCE_API];
                    return (new DataWrap($this, $channel))->channel();
                }
            )->all(), null
        );
    }

    public function getOperators(mixed $cursor = null): IterableData
    {
        $mainApiPaginator = $this->getMainApiClient()->all('users', $cursor ?? 1, ['query' => ['filter[active]' => 1]]
        );
        $mainManagers = $mainApiPaginator->data['users'];
        $botApiManagers = [];
        foreach ($mainManagers as $key => $mainManager) {
            if (!isset($mainManager['mgUserId'])) {
                continue;
            }
            $botApiManagers[$key]['external_id'] = $mainManager['id'];
            $botApiManagers[$key]['id'] = $mainManager['mgUserId'];
            $botApiManagers[$key]['avatar'] = $mainManager['photoUrl'] ?? null;
            $botApiManagers[$key]['name'] = $mainManager['name'] ?? null;
            $botApiManagers[$key]['first_name'] = $mainManager['firstName'] ?? null;
            $botApiManagers[$key]['last_name'] = $mainManager['lastName'] ?? null;
            $botApiManagers[$key]['email'] = $mainManager['email'] ?? null;
            $botApiManagers[$key]['type'] = AccountType::MANAGER;
        }

        $botsAndManagers = [];
        if ($this->isCompleteInstance()) {
            $botApiBots = $this->getBotApiClient()->get('bots', [ 'query' => [ 'active' => 1 ] ]);
            foreach ($botApiBots as $key => $bot) {
                $botsAndManagers[$key]['id'] = $bot['id'];
                $botsAndManagers[$key]['avatar'] = $bot['avatar_url'] ?? null;
                $botsAndManagers[$key]['name'] = $bot['name'] ?? null;
                $botsAndManagers[$key]['type'] = AccountType::BOT;
            }
            $botsAndManagers = array_merge($botsAndManagers, $botApiManagers);
        }
        return new IterableData(
            collect(
                $botsAndManagers
            )->map(
                function (array $manager) {
                    $manager = [DataWrap::DATA_KEY => $manager, DataWrap::DATA_SOURCE_KEY => DataWrap::DATA_SOURCE_API];
                    return (new DataWrap($this, $manager))->user();
                }
            )->all(), $mainApiPaginator->cursor
        );
    }

    public function getOperatorGroups(mixed $cursor = null): IterableData
    {
        return with(
            $this->getMainApiClient()->all('user-groups', $cursor ?? 1),
            fn(IterableData $data) => new IterableData(
                collect(
                    $data->data['groups']
                )->map(
                    fn(array $group) => new OperatorGroup(
                        externalId: $group['id'],
                        name: $group['name'],
                        extraData: [
                            ExtraDataProps::IS_GROUP_DELIVERY_MEN => $group['isDeliveryMen'] ?? null,
                            ExtraDataProps::IS_GROUP_MANAGER => $group['isManager'] ?? null,
                            ExtraDataProps::USER_GROUP_CODE => $group['code'] ?? null,
                        ]
                    )
                )->all(), $data->cursor
            )
        );
    }

    public function getCommands(): \BmPlatform\Abstraction\Interfaces\Commands\Commands
    {
        if (isset($this->commands)) {
            return $this->commands;
        }

        return $this->commands = new Commands($this);
    }

    public function getMainApiClient(): MainApiClient
    {
        if (isset($this->mainApiClient)) {
            return $this->mainApiClient;
        }

        return $this->mainApiClient = new MainApiClient(
            $this->user->getExtraData()[ExtraDataProps::MAIN_API_URL],
            $this->user->getExtraData()[ExtraDataProps::MAIN_API_KEY],
            [
                'appInstance' => $this->user,
            ]
        );
    }

    public function getBotApiClient(): BotApiClient
    {
        if (isset($this->botApiClient)) {
            return $this->botApiClient;
        }

        return $this->botApiClient = new BotApiClient(
            $this->user->getExtraData()[ExtraDataProps::MG_BOT_API_URL],
            $this->user->getExtraData()[ExtraDataProps::MG_BOT_API_TOKEN],
            [ 'appInstance' => $this->user ]
        );
    }

    public function status(): AppExternalStatus
    {
        try {
            $module = $this->getMainApiClient()->get('integration-modules/:bot-code', [
                'params' => [ 'bot-code' => Config::get('retailcrm.applications.'.$this->getAppType().'.code') ]
            ]);
        }

        catch (ErrorException $e) {
            if ($e->errorCode == ErrorCode::InsufficientFunds) {
                return new AppExternalStatus(AppStatus::Frozen);
            }

            throw $e;
        }

        $frozen = $module['integrationModule']['freeze']
            ?? throw new ErrorException(ErrorCode::UnexpectedServerError, 'Unexpected payload received: flag freeze is absent');

        $active = $module['integrationModule']['active']
            ?? throw new ErrorException(ErrorCode::UnexpectedServerError, 'Unexpected payload received: flag active is absent');

        return Utils::mapAppStatus((bool)$active, (bool)$frozen, $this->getAppType());
    }

    public function allowedTransports(): array
    {
        return array_keys($this->config->get('retailcrm.messenger_types'));
    }

    public function activate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))();
    }

    public function deactivate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))(enable: false);
    }

    protected function isCompleteInstance(): bool
    {
        return isset($this->user->getExtraData()[ExtraDataProps::MG_BOT_API_URL]);
    }

    public function getAppType(): string
    {
        $id = explode(':', $this->user->getExternalId());

        return $id[1] ?? 'main';
    }

    public function freshTemporaryData(TemporaryData $data): TemporaryData
    {
        $builder = new CustomFieldsBuilder($this->getMainApiClient());

        return new TemporaryData([ 'cf' => $builder() ], Carbon::now()->addDay());
    }

    public function resolveCustomer(Contact $contact): ?array
    {
        $resp = $this->getMainApiClient()->get('customers', [
            'query' => [ 'filter[mgCustomerId]' => $contact->getExternalId() ],
        ]);

        return $resp['customers'][0] ?? null;
    }
}
