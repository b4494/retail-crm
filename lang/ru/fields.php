<?php

return [
    'account_type_user' => 'Пользователь',
    'account_type_bot' => 'Бот',
    'account_type' => 'Тип',
    'customer' => 'Клиент',
    'type' => 'Тип клиента',
    'id' => 'Идентификатор в RetailCRM',
    'externalId' => 'Внешний идентификатор',
    'isContact' => 'Контактное лицо, не имеет заказов',
    'createdAt' => 'Дата создания клиента',
    'managerId' => 'Идентификатор менеджера клиента',
    'vip' => 'Важный клиент',
    'bad' => 'Плохой клиент',
    'site' => 'Магазин, из которого пришел клиент',
    'firstClientId' => 'Первая метка клиента Google Analytics',
    'lastClientId' => 'Последняя метка клиента Google Analytics',
    'customFields' => 'Пользовательские поля',
    'avgMarginSumm' => 'Средняя валовая прибыль по заказам',
    'marginSumm' => 'Пожизненная ценность',
    'totalSumm' => 'Общая сумма заказов',
    'avgSumm' => 'Средняя сумма заказа',
    'ordersCount' => 'Количество заказов',
    'costSumm' => 'Сумма расходов',
    'personalDiscount' => 'Персональная скидка',
    'cumulativeDiscount' => 'Накопительная скидка',
    'discountCardNumber' => 'Номер дисконтной карты',
    'maturationTime' => 'Время созревания(в секундах)',
    'firstName' => 'Имя',
    'lastName' => 'Фамилия',
    'patronymic' => 'Отчество',
    'sex' => 'Пол',
    'presumableSex' => 'Предполагаемый пол',
    'email' => 'Адрес электронной почты',
    'emailMarketingUnsubscribedAt' => 'Дата отписки от почтовой рассылки',
    'birthday' => 'День рождения',
    'photoUrl' => 'Ссылка на фото',

    // complex variables
    'phones' => 'Номера телефонов',
    'phones' . '.number' => 'Номер телефона',
    'phone' => 'Номер телефона',
    'phoneNotice' => 'Список номеров телефонов будет заменен на один указанный в этом поле номер',

    'contragent' => 'Реквизиты',

    'contragent.contragentType' => 'Тип контрагента',
    'contragent.legalName' => 'Полное наименование',
    'contragent.legalAddress' => 'Адрес регистрации',
    'contragent.INN' => 'ИНН',
    'contragent.OKPO' => 'ОКПО',
    'contragent.KPP' => 'КПП',
    'contragent.OGRN' => 'ОГРН',
    'contragent.OGRNIP' => 'ОГРНИП',
    'contragent.certificateNumber' => 'Номер свидетельства',
    'contragent.certificateDate' => 'Дата свидетельства',
    'contragent.BIK' => 'БИК',
    'contragent.bank' => 'Банк',
    'contragent.bankAddress' => 'Адрес банка',
    'contragent.corrAccount' => 'Корр. счет',
    'contragent.bankAccount' => 'Расчетный счет',

    'address' => 'Адрес',

    'address.id' => 'Идентификатор адреса',
    'address.index' => 'Индекс',
    'address.countryIso' => 'ISO код страны',
    'address.region' => 'Регион',
    'address.regionId' => 'Идентификатор региона в Geohelper',
    'address.city' => 'Город',
    'address.cityId' => 'Идентификатор города в Geohelper',
    'address.cityType' => 'Тип населенного пункта',
    'address.street' => 'Улица',
    'address.streetId' => 'Идентификатор улицы в Geohelper',
    'address.streetType' => 'Тип улицы',
    'address.building' => 'Дом',
    'address.flat' => 'Номер квартиры/офиса',
    'address.floor' => 'Этаж',
    'address.block' => 'Подъезд',
    'address.house' => 'Строение',
    'address.housing' => 'Корпус',
    'address.metro' => 'Метро',
    'address.notes' => 'Примечание по адресу',
    'address.text' => 'Адрес в текстовом виде',
    'address.externalId' => 'Внешний идентификатор',
    'address.name' => 'Наименование адреса',

    'segments' => 'Сегменты',
    'segments.isDynamic' => 'Автоматически пересчитывается',
    'segments.customersCount' => 'Количество клиентов',
    'segments.active' => 'Активность сегмента',

    'source' => 'Источник клиента',

    'source.source' => 'Источник',
    'source.medium' => 'Канал',
    'source.campaign' => 'Кампания',
    'source.keyword' => 'Ключевое слово',
    'source.content' => 'Содержание кампании',

    'chat.mgCustomer' => 'Клиент',
    'chat.mgCustomer.id' => 'Идентификатор',
    'chat.mgCustomer.externalId' => 'MessageGateway идентификатор',
    'chat.mgCustomer.mgChannel' => 'Канал',
    'chat.mgCustomer.mgChannel.id' => 'Идентификатор',
    'chat.mgCustomer.mgChannel.externalId' => 'MessageGateway идентификатор',
    'chat.mgCustomer.mgChannel.type' => 'Тип',
    'chat.mgCustomer.mgChannel.active' => 'Активен',
    'chat.mgCustomer.mgChannel.name' => 'Название',
    'integration' => 'Интеграция',
    'tagName' => 'Название тега',
    'addTags' => 'Добавить теги',
    'removeTags' => 'Убрать теги',
    'addNote' => 'Добавить заметку',
];
