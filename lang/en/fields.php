<?php

return [
    'account_type_user' => 'User',
    'account_type_bot' => 'Bot',
    'account_type' => 'Type',
    'customer' => 'Customer',
    'type' => 'Client type',
    'id' => 'Id in RetailCRM',
    'externalId' => 'External id',
    'isContact' => 'Contact person, does not have orders',
    'createdAt' => 'Date of client creation',
    'managerId' => 'Client manager id',
    'vip' => 'Important(VIP) client',
    'bad' => 'Bad client',
    'site' => 'Customer came from shop',
    'firstClientId' => 'First client Google Analytics id',
    'lastClientId' => 'Last client Google Analytics id',
    'customFields' => 'Custom fields',
    'avgMarginSumm' => 'Average margin sum',
    'marginSumm' => 'Margin sum',
    'totalSumm' => 'Orders total',
    'avgSumm' => 'Average order sum',
    'ordersCount' => 'Orders count',
    'costSumm' => 'Costs sum',
    'personalDiscount' => 'Personal discount',
    'cumulativeDiscount' => 'Cumulative discount',
    'discountCardNumber' => 'Discount card number',
    'maturationTime' => 'Maturation time(in seconds)',
    'firstName' => 'First name',
    'lastName' => 'Last name',
    'patronymic' => 'Patronymic',
    'sex' => 'Sex',
    'presumableSex' => 'Presumable sex',
    'email' => 'Email address',
    'emailMarketingUnsubscribedAt' => 'Date when client unsubscribed from mail',
    'birthday' => 'Birthday',
    'photoUrl' => 'Photo url',

    // complex variables
    'phones' => 'Phone numbers',
    'phones.number' => 'Phone number',
    'phone' => 'Phone number',
    'phoneNotice' => 'Several numbers are overwritten with single value you specify in this field',

    'contragent' => 'Counterparty',

    'contragent.contragentType' => 'Type',
    'contragent.legalName' => 'Legal name',
    'contragent.legalAddress' => 'Legal address',
    'contragent.INN' => 'INN',
    'contragent.OKPO' => 'OKPO',
    'contragent.KPP' => 'KPP',
    'contragent.OGRN' => 'OGRN',
    'contragent.OGRNIP' => 'OGRNIP',
    'contragent.certificateNumber' => 'Certificate number',
    'contragent.certificateDate' => 'Certificate date',
    'contragent.BIK' => 'BIK',
    'contragent.bank' => 'Bank',
    'contragent.bankAddress' => 'Bank address',
    'contragent.corrAccount' => 'Corr. account',
    'contragent.bankAccount' => 'Bank account',

    'address' => 'Address',

    'address.id' => 'Id',
    'address.index' => 'Index',
    'address.countryIso' => 'ISO country code',
    'address.region' => 'Region',
    'address.regionId' => 'Geohelper region id',
    'address.city' => 'City',
    'address.cityId' => 'Geohelper city id',
    'address.cityType' => 'City type',
    'address.street' => 'Street',
    'address.streetId' => 'Geohelper street id',
    'address.streetType' => 'Street type',
    'address.building' => 'Building',
    'address.flat' => 'Flat/office',
    'address.floor' => 'Floor',
    'address.block' => 'Block',
    'address.house' => 'House',
    'address.housing' => 'Housing',
    'address.metro' => 'Metro',
    'address.notes' => 'Notes',
    'address.text' => 'Textual address',
    'address.externalId' => 'External id',
    'address.name' => 'Name',

    'segments' => 'Segments',

    'segments.id' => 'Internal id',
    'segments.code' => 'Code',
    'segments.name' => 'Name',
    'segments.createdAt' => 'Creation date',
    'segments.isDynamic' => 'Automatically recalculated',
    'segments.customersCount' => 'Customers count',
    'segments.active' => 'Active',

    'source' => 'Client source',

    'source.source' => 'Source',
    'source.medium' => 'Channel',
    'source.campaign' => 'Campaign',
    'source.keyword' => 'Keyword',
    'source.content' => 'Campaign content',

    'chat.mgCustomer' => 'Client',
    'chat.mgCustomer.id' => 'Id',
    'chat.mgCustomer.externalId' => 'External id',
    'chat.mgCustomer.mgChannel' => 'Channel',
    'chat.mgCustomer.mgChannel.id' => 'Id',
    'chat.mgCustomer.mgChannel.externalId' => 'External id',
    'chat.mgCustomer.mgChannel.type' => 'Type',
    'chat.mgCustomer.mgChannel.active' => 'Active',
    'chat.mgCustomer.mgChannel.name' => 'Name',
    'integration' => 'Integration',
    'tagName' => 'Tag name',
    'addTags' => 'Add tags',
    'removeTags' => 'Remove tags',
    'addNote' => 'Add note',
];
