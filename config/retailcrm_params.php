<?php

return [
    'required' => ['ws_address', 'bot_code', 'partner_profile_secret'],

    'ws_address' => env('RETAILCRM_WS_ADDRESS'),
    // expects a full url with path http://localhost/integrations/retailcrm/webhook
    'wh_callback_url_base_path' => env('RETAILCRM_CALLBACK_URL_BASE_PATH'),

    // set to false for easier debugging
    'disable_validation' => env('RETAILCRM_DISABLE_VALIDATION', false),

    'scopes' => [
        'user_read',
        'customer_read',
        'customer_write',
        'integration_write',
        'integration_read',
        'order_read',
        'order_write',
        'reference_read',
        'custom_fields_read',
        'notification_write',
    ]
];


