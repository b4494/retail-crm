<?php

return [
    'applications' => json_decode(env('RETAILCRM_APPS', '{"main":{"code":"","secret":"","botName":""},"catalog":{"code":"","secret":"","botName":""}}'), true),

    'title' => 'RetailCRM',

    'events' => [
        'inboxFlags' => [
            'newChatCreated' => true,
            'newTicketOpened' => true,
            'externalPostComment' => false,
        ],
        'outboxSent' => true,
        'newContactChatAdded' => false,
        'chatTicketClosed' => true,
        'chatTransferred' => true,
        'messageStatusChanged' => true,
        'chatTagAttached' => false,
        'chatTagDetached' => false,
        'chatTicketTagAttached' => false,
        'chatTicketTagDetached' => false,
        'contactDataUpdated' => false,
    ],

    'features' => [
        'operators' => [
            'read' => true,
            'transfer' => true,
        ],

        'operatorGroups' => [
            'read' => true,
            'transfer' => false,
        ],

        'tags' => [
            'asStrings' => true,
//            'attach' => true,
//            'detach' => true,
        ],
    ],

    'data' => [
        'contact' => [
            'phone' => false,
            'email' => false,
            'messengerId' => true,
        ],

        'chat' => [
            'messengerId' => true,
        ],

        'operator' => [
            'phone' => false,
        ],
    ],

    'actions' => [
        'update_contact_data' => [
            'supported' => true,
            'editableFields' => [
                'type' => 'object',
                'properties' => [
                    'phone' => [
                        'type' => 'phone',
                        'title' => 'retailcrm::fields.phone',
                        'description' => 'retailcrm::fields.phoneNotice',
                    ],
                    'firstName' => [
                        'type' => 'string',
                        'title' => 'retailcrm::fields.firstName',
                    ],
                    'lastName' => [
                        'type' => 'string',
                        'title' => 'retailcrm::fields.lastName',
                    ],
                    'patronymic' => [
                        'type' => 'string',
                        'title' => 'retailcrm::fields.patronymic',
                    ],
                    'email' => [
                        'type' => 'string',
                        'title' => 'retailcrm::fields.email',
                    ],
                    'birthday' => [
                        'type' => 'date',
                        'title' => 'retailcrm::fields.birthday',
                    ],
                    'photoUrl' => [
                        'type' => 'string',
                        'title' => 'retailcrm::fields.photoUrl',
                    ],
                    'source_source' => [
                        'type' => 'string',
                        'title' => 'UTM Source',
                    ],
                    'source_medium' => [
                        'type' => 'string',
                        'title' => 'UTM Medium',
                    ],
                    'source_campaign' => [
                        'type' => 'string',
                        'title' => 'UTM Campaign',
                    ],
                    'source_keyword' => [
                        'type' => 'string',
                        'title' => 'UTM Keyword',
                    ],
                    'source_content' => [
                        'type' => 'string',
                        'title' => 'UTM Content',
                    ],
                    'addTags' => [
                        'type' => 'list',
                        'title' => 'retailcrm::fields.addTags',
                        'keyLabel' => 'retailcrm::fields.tagName',
                    ],
                    'removeTags' => [
                        'type' => 'list',
                        'title' => 'retailcrm::fields.removeTags',
                        'keyLabel' => 'retailcrm::fields.tagName',
                    ],
                    'addNote' => [
                        'type' => 'text',
                        'title' => 'retailcrm::fields.addNote',
                    ],
                ],
                'processor' => \BmPlatform\RetailCRM\AppendCustomFields::class,
            ]
        ]
    ],

    'messenger_types' => [
        'default' => [
            'name' => 'Unknown messenger',

            'quick_replies' => [
                'text' => true,
                'text_max_length' => 20,
                'max_count' => 32,
            ],

            'text' => [
                'max_length' => 4096,
            ],

            'image' => [
                'enabled' => true,
                'mime' => ['image/jpeg', 'image/png', 'image/gif'],
                'max_file_size' => 5 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 1000,
            ],

            'video' => [
                'enabled' => true,
                'mime' => ['video/mp4', 'video/3gpp'],
                'max_file_size' => 50 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 1000,
            ],

            'document' => [
                'enabled' => true,
                'mime' => ['*'],
                'max_file_size' => 50 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 1000,
            ],

            'audio' => [
                'enabled' => true,
                'mime' => ['audio/ogg', 'audio/mpeg', 'audio/mp4', 'audio/aac'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => false,
                'caption_max_length' => 1000,
            ],
        ],

        'telegram' => [
            'extends' => 'default',
            'name' => 'Telegram',
            'internal_type' => 'telegram',
        ],
        'fbmessenger' => [
            'extends' => 'default',
            'name' => 'Facebook messenger',
            'internal_type' => 'facebook',
        ],
        'viber' => [
            'extends' => 'default',
            'name' => 'Viber',
            'internal_type' => 'viber_public',
        ],
        'whatsapp' => [
            'extends' => 'default',
            'name' => 'WhatsApp',
            'internal_type' => 'whatsapp',
        ],
        'skype' => [
            'extends' => 'default',
            'name' => 'Skype',
        ],
        'vk' => [
            'extends' => 'default',
            'name' => 'VK',
            'internal_type' => 'vk',
        ],
        'instagram' => [
            'extends' => 'default',
            'name' => 'Instagram',
            'internal_type' => 'instagram',
        ],
        'consultant' => [
            'extends' => 'default',
            'name' => 'Consultant',
        ],
        'yandex_chat' => [
            'extends' => 'default',
            'name' => 'YandexChat',
        ],
        'custom' => [
            'extends' => 'default',
            'name' => 'Custom',
        ],
    ],
];


