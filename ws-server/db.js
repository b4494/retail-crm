const { logWithTime } = require('./utils');
require('dotenv').config();

var sqlite3 = require('sqlite3').verbose();

let STORAGE_PATH = process.env.STORAGE_PATH;
if (STORAGE_PATH && !STORAGE_PATH.endsWith('/')) {
    STORAGE_PATH += '/';
}
const DB_SOURCE = STORAGE_PATH + "retailcrm_connections_storage"

const handleError = (err) => {
    if (err) {
        console.error('Database error! Try running migrations. Error contents: ' + err.message);
    }
}

class ConnectionsDatabase {
    db;

    constructor(DB_STORAGE_PATH) {
        this.db = new sqlite3.Database(DB_STORAGE_PATH, (err) => {
            if (err) {
                console.error(err.message);
                throw err;
            }
            logWithTime('Connected to the SQLite database.')
        });
    }

    upsertConnection(connection, isActive, callback) {
        logWithTime('upserting connection: ', connection);
        const values = [connection.webhookUrl, connection.token, connection.host, connection.apiVersion, isActive, connection.domain, isActive, connection.host, connection.apiVersion, connection.domain];
        this.db.run(`INSERT INTO connections (webhook_url, bot_token, host, api_version, should_be_active, \`domain\`) VALUES (?,?,?,?,?,?) ON CONFLICT DO UPDATE SET should_be_active = ?, host = ?, api_version = ?, \`domain\` = ?`, values, function (err) {
            err && handleError(err);
            typeof callback === 'function' && callback.call(this, err);
        });
    }

    executeOnEachConnection(isActive, callback) {
        return this.db.all('SELECT id, webhook_url as webhookUrl, bot_token as token, host, api_version as apiVersion, `domain` FROM connections WHERE should_be_active = ' + (isActive ? 1 : 0), (err, rows) => {
            handleError(err);
            rows.length === 0 ? logWithTime('No ' + (isActive ? 'active' : 'inactive') + ' connections are present in the database.') : rows.forEach(row => callback(row));
        });
    }

    writeCommentOnActiveConnection(connectionId, comment = null) {
        this.db.run('INSERT INTO connections_status_history (connection_id, should_be_active, comment) VALUES (?, ?, ?)', [connectionId, 1, comment], (err, result) => {
            handleError(err);
        });
    }

    changeActivityStatus (connection, isActive) {
        this.db.run('UPDATE connections SET should_be_active = ? WHERE `domain` = ?', [ (isActive ? 1 : 0), connection.domain ], (err, result) => {
            handleError(err);
        });

        logWithTime('Connection with wh url ' + connection.webhookUrl + ' was set to status ' + (isActive ? 'active' : 'inactive'));
    }
}

let connectionsDatabase = new ConnectionsDatabase(DB_SOURCE);

module.exports = connectionsDatabase;
