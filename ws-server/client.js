const { logWithTime, getMicrosecondsISOString } = require('./utils');
const WebSocket = require('ws');
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
let id = 0;

/*
    config = {
        host: '',
        token: '',
        apiVersion: '',
        webhookUrl: '',
        connectMaxTimeout: 90, //in seconds
        reconnectMaxTimeout: 5, //in seconds
        events: [] //all events need to be listen
    }
*/
module.exports = class {
  config;
  connection;
  id;
  tries = 0;
  timer;
  closedByUser = 0;
  isAlive = false;
  isValid = true;
  isConnected = false;

  constructor (_config) {
    this.config = {
      connectMaxTimeout: 90,
      reconnectMaxTimeout: 5,
      maxRetries: 10,
      events: [
        'message_new', 'message_updated', 'message_deleted', 'dialog_opened', 'dialog_closed', 'dialog_assign',
        'chat_created', 'chat_updated', 'user_joined_chat', 'user_left_chat', 'user_updated'
      ],
    };
    this.id = ++id;

    Object.assign(this.config, _config);

    this.reconnect();
  }

  log (msg) {
    logWithTime(`cn=${this.config.domain} id=${this.id} ${msg}`);
  }

  init() {
    const url = this.getEndpointUrl();

    this.log(`connecting to ${url}`);

    this.connection = new WebSocket(url, {
      headers: this.getAuthHeaders(),
    });

    this.connection.on('open', (ws) => {
      this.tries = 0;
      this.isAlive = true;
      this.isConnected = true;

      this.log('connected');

      this.connection.onclose = (event) => {
        this.isConnected = false;

        if (this.closedByUser) {
          this.log('connection was closed by the user');

          return;
        }

        this.log(`connection was closed: ${event.reason}`);
        this.reconnect();
      };

      this.connection.on('message', data => {
        const start = getMicrosecondsISOString();

        fetch(this.config.webhookUrl, {
          method: 'post',
          body: data,
          headers: {
            'Content-Type': 'application/json',
            'X-Receive-Time': start,
          }
        }).catch((error) => {
          this.log(`[CRITICAL] ${error.message} while sending ${data}`);
        });
      });
    });

    this.connection.on('pong', () => this.isAlive = true);

    this.connection.on('error', (event) => {
      const isDisabled = event.message === 'Unexpected server response: 403';

      if (!isDisabled && (event.code >= 500 || event.message.match(/ETIMEDOUT|EAI_AGAIN|Unexpected server response/))) {
        this.reconnect();

        return;
      }

      this.log(isDisabled ? `bot is disabled` : `connection got error: ${event.message}, code: ${event.code}`);

      this.isValid = false;

      this.deactivate();
    });
  }

  reconnect() {
    if (this.closedByUser) return;

    if (this.tries >= this.config.maxRetries) {
      //log info and push info to server in order of remove this integration from list
      this.log(`can't connect to socket more than ${this.config.maxRetries} times`);

      this.connection?.terminate();

      return;
    }

    if (this.timer) {
      return;
    }

    this.isAlive = false;

    const delay = Math.floor(Math.exp(this.tries));

    this.log(`try to reconnect in ${delay} sec`);

    this.timer = setTimeout(() => {
      this.timer = null;
      this.tries++;
      this.init();
    }, delay * 1000);
  }

  deactivate () {
    if (!this.connection) return;

    this.closedByUser = true;
    this.connection.terminate();
    this.connection = null;
    this.isAlive = false;
    this.log('Connection was terminated');
  }

  heartbeat () {
    if (!this.connection || !this.isConnected) return;

    if (this.isAlive) {
      this.isAlive = false;
      this.connection.ping();
    } else {
      this.connection.terminate();
      this.connection = null;
      this.log('Connection was terminated because did not receive pong message');
    }
  }

  getEndpointUrl() {
    return 'wss://' + this.config.host + '/api/bot/' + this.config.apiVersion + '/ws?events=' + this.config.events.join();
  }

  getAuthHeaders() {
    return {'X-Bot-Token': this.config.token};
  }
}