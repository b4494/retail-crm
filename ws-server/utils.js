const microtime = require('microtime');

function getMicrosecondsISOString() {
  let microEpoch = microtime.now();
  let miliEpoch = microEpoch/1000;
  let micro = microEpoch % 1000;
  let isoDate = new Date(miliEpoch).toISOString();
  return isoDate.replace('Z', micro + 'Z');
}

function logWithTime (message) {
  console.log(`[${getMicrosecondsISOString()}] ${message}`);
}

module.exports = { logWithTime, getMicrosecondsISOString };