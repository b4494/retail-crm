let connectionsDatabase = require('./db.js');

connectionsDatabase.db.run(`CREATE TABLE IF NOT EXISTS connections
                            (
                                id               INTEGER PRIMARY KEY AUTOINCREMENT,
                                \`domain\`       varchar(512) not null,
                                webhook_url      VARCHAR(512) NOT NULL,
                                bot_token        VARCHAR(128) NOT NULL,
                                host             VARCHAR(512) NOT NULL,
                                api_version      VARCHAR(20)  NOT NULL,
                                should_be_active BOOLEAN DEFAULT true,
                                created_at       TEXT    DEFAULT CURRENT_TIMESTAMP,
                                updated_at       TEXT    DEFAULT CURRENT_TIMESTAMP,
                                UNIQUE (webhook_url, bot_token, api_version)
                            )`, (err, result) => {
    if (err) {
        console.error(err.message);
        throw err;
    }
    connectionsDatabase.db.run(`CREATE TRIGGER IF NOT EXISTS connections_updated_at AFTER UPDATE ON connections
                            BEGIN
                                UPDATE connections SET updated_at = datetime('now') WHERE id = NEW.id;
                            END;`);
});


connectionsDatabase.db.run(`CREATE TABLE IF NOT EXISTS connections_status_history
                            (
                                id               INTEGER PRIMARY KEY AUTOINCREMENT,
                                connection_id    BIGINT  NOT NULL,
                                should_be_active BOOLEAN NOT NULL,
                                comment          TEXT,
                                created_at       TEXT DEFAULT CURRENT_TIMESTAMP,
                                FOREIGN KEY (connection_id) REFERENCES connections (id)
                            )`, (err, result) => {
    if (err) {
        console.error(err.message);
        throw err;
    }
    connectionsDatabase.db.run(`CREATE TRIGGER IF NOT EXISTS connections_status_history_write AFTER UPDATE OF should_be_active ON connections
                            BEGIN
                                INSERT INTO connections_status_history (connection_id, should_be_active) VALUES (NEW.id, NEW.should_be_active);
                            END;`);
});