
require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const connectionsDatabase = require('./db.js');

const app = express();
const integrations = new Map();
const RetailCRMEventHandler = require('./client');
const logWithTime = require('./utils').logWithTime;

const errorHandler = (error, request, response, next) => {
    // Error handling middleware functionality
    logWithTime(`error ${error.message}`); // log the error
    const status = error.status || 400;
    // send back an easily understandable error message to the caller
    response.status(status).json({status: 'error', message: error.message});
}

app.use(morgan('combined'));
app.use(express.json());
app.use(errorHandler);

app.get('/', (req, res) => {
    res.send('works');
});

app.post('/add', (req, res) => {
    try {
        const key = req.body.domain;

        if (integrations.has(key)) {
            integrations.get(key).deactivate();
        }

        connectionsDatabase.upsertConnection(req.body, 0, function (err) {
            if (err) {
                res.json({ status: 'error', message: 'database error' });

                return;
            }

            const data = { id: this.lastID, ...req.body };

            const integration = new RetailCRMEventHandler(data);

            integrations.set(key, integration);

            connectionsDatabase.changeActivityStatus(req.body, 1);

            res.json({ status: 'success', data });
        });

    } catch (error) {
        // next(error) // calling next error handling middleware
        logWithTime('ERROR: ', error);
    }
});

app.put('/remove', (req, res) => {
    try {
        let integration = integrations.get(req.body.domain);

        if (!integration) {
            res.status(404);
            res.send('connection is not activated');

            return;
        }

        integration.deactivate();

        integrations.delete(req.body.domain);

        connectionsDatabase.changeActivityStatus(req.body, 0);

        res.send(req.body.token + ' deactivated');
    } catch (error) {
        logWithTime('ERROR: ', error);
    }
});

init(connectionsDatabase);

const PORT = process.env.PORT || 5005;

app.listen(PORT, () => {
    logWithTime(`RetailCRM Event server started on on port ${PORT}`)
});

function init (db) {
    const connect = (connection) => {
        let integration = new RetailCRMEventHandler(connection);

        const key = integration.config.domain;

        if (integrations.has(key)) {
            console.error(`Duplicate integration key ${key}!`);

            return;
        }

        integrations.set(key, integration);

        logWithTime('Successful reconnect for connection ' + connection.id);

        db.writeCommentOnActiveConnection(connection.id, 'Reconnected on server start.');
    }

    db.executeOnEachConnection(1, connect);

    setInterval(heartbeat, 10 * 1000);
}

function heartbeat () {
    integrations.forEach(conn => {
        if (conn.isValid) {
            conn.heartbeat()
        } else {
            integrations.delete(conn.config.domain);

            connectionsDatabase.changeActivityStatus(conn.config, 0);
        }
    });
}